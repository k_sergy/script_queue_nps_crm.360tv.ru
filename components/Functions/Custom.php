<?php

/**
 * html select options
 */
function htmlSelectOptions($arr, $selected = null){

	$options = array();
	foreach( (array)$arr as $key=>$val ){
		if (is_array($val)) {
			$val = (object)$val;
		}
		if( is_object( $val )){
			if( isset( $val->title )){
				$val = $val->title;
			} else {
				$val = $val->name;
			}
		}
		$sel = '';
		if (!is_null($selected)) {
			if( !is_array( $selected )){
				$selected = array( $selected );
			}
			if( in_array( $key, $selected )){
				$sel = ' selected';
			}
		}
		$options[]= '<option value="'.$key.'"'.$sel.'>'.$val.'</option>';
	}
	return implode( "\r\n",$options );
}

/**
 * День недели на русском
 */
function weekRU($date)
{
    $a = array('/Sun/', '/Mon/', '/Tue/', '/Wed/', '/Thu/', '/Fri/', '/Sat/');
    $b = array('вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб');

    $date = preg_replace($a, $b, $date);
    return $date;
}

/**
 * Месяц на русском
 */
function monthRU($date, $type = 0)
{
    $a = array('/January/', '/February/', '/March/', '/April/', '/May/', '/June/', '/July/', '/August/', '/September/', '/October/', '/November/', '/December/');
    $b = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
    if ($type == 1) {
        $b = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    }
    $date = preg_replace($a, $b, $date);
    return $date;
}

/**
 * Вывод даты
 */
function formatDate( $t )
{
	$time = time();
	$tm = date('H:i', $t);
	$d = date('d', $t);
	$m = date('m', $t);
	$y = date('Y', $t);
	$last = round(($time - $t)/60);
	if( $last < 1 ) return "прямо сейчас";
	if( $last < 55 ) return "$last мин назад";
	elseif($d.$m.$y == date('dmY',$time)) return "сегодня в $tm";
	elseif($d.$m.$y == date('dmY', strtotime('-1 day'))) return "вчера в $tm";
	elseif( ($time - $t) > 31536000 ) return "больше года назад";
	elseif($y != date('Y',$time)) return monthRU(date('j F Y, H:i', $t));
	else return monthRU(date('j F, H:i', $t));
}

/**
 * Получение даты следующего рабочего дня
 */
function getNextWorkDayDate($time)
{
	return date('Y-m-d 23:59:59', getWorkTime(1, $time));
}

/**
 * Получение метки времени на задачу
 */
function getWorkTime($hours, $time = null)
{
	if (is_null($time)) {
		$time = App\Core::timestamp();
	}
	//echo "<br>\n";
	//echo 'Текущая дата: '.monthRU(date('d F Y, H:i:s', $time))."<br>\n";
	//echo 'Часов на задачу: '.$hours."<br>\n";

	$h = (int)date('H',$time);
	$m = (int)date('i',$time);
	//echo 'Hours: '.$h."<br>\n";
	//echo 'Minut: '.$m."<br>\n";
	
	$endstamp = $time+$hours*60*60;
	$stamp = $endstamp-$time;
	//echo 'Stamp: '.$stamp."<br>\n";
	
	$day_diff_time = 86400-($time-strtotime(date('Y-m-d',$time)));
	//echo 'Day diff time: '.$day_diff_time."<br>\n";
	$diff_stamp = $time;
	//echo 'Day diff date: '.monthRU(date('d F Y, H:i:s', $diff_stamp))."<br>\n";
	
	$from = 9;
	$to = 16;
	$max = $to-$from;
	if ($h>$to) {
		$time+=$day_diff_time;
		$new_date = date('Y-m-d '.$from.':00:00',$time);
		//echo 'New date calc: '.$new_date."<br>\n";
		return getWorkTime($hours, strtotime($new_date));
	} elseif ($h<$from) {
		$new_date = date('Y-m-d '.$from.':00:00',$time);
		//echo 'New date calc: '.$new_date."<br>\n";
		return getWorkTime($hours, strtotime($new_date));
	}
	//echo 'Hours calc: '.$h."<br>\n";
	//echo 'Minut calc: '.$m."<br>\n";
	
	$today_end_minut = 60-$m;
	$today_end_hours = $to-$h;
	if ($today_end_minut == 60) {
		$today_end_minut = 0;
	} else {
		$today_end_hours-=1;
	}
	//echo 'Today end hours: '.$today_end_hours."<br>\n";
	//echo 'Today end minut: '.$today_end_minut."<br>\n";
	$without_hours = $hours-$today_end_hours;
	//echo 'Hours without today: '.$without_hours."<br>\n";
	
	$daynum = 1;
	$without_days = 0;
	$end_day_hours = $hours;
	if ($without_hours > 0) {
		$without_days = ceil($without_hours/$max);
	}
	$daynum+= $without_days;
	//echo 'Without day num: '.$without_days."<br>\n";
	
	$days = array();
	if ($hours > $today_end_hours) {
		$days[date('Y-m-d', $diff_stamp)] = $today_end_hours;
		$end_day_hours-= $today_end_hours;
	} else {
		$days[date('Y-m-d', $diff_stamp)] = $hours;
		$end_day_hours-= $hours;
	}
	$diff_stamp = $time+$day_diff_time;
	//echo 'Day diff date next: '.monthRU(date('d F Y, H:i:s', $diff_stamp))."<br>\n";
	
	for ($i=2; $i<=$daynum; $i++) {
		
		//echo 'i: '.$i.', daynum:'.$daynum."<br>\n";
		while (date('N', $diff_stamp) > 5 ) {
			$days[date('Y-m-d', $diff_stamp)] = 24;
			$diff_stamp+= 86400;
		}
		if ($i == $daynum) {
			$days[date('Y-m-d', $diff_stamp)] = $end_day_hours;
		} else {
			$days[date('Y-m-d', $diff_stamp)] = $max;
			$end_day_hours-= $max;
			$diff_stamp+= 86400;
		}
	}
	//print_r($days);
	//echo "<br>\n";
	
	end($days);
	$end_date = key($days);
	$end_hour = (int)$days[$end_date];
	if (count($days) == 1) {
		$end_hour+= $h;
	} else {
		$end_hour+= $from;
		if ($m > 0) {
			$end_hour-= 1;
		}
	}
	$end_datetime = $end_date.' '.($end_hour).':'.$m.':00';
	//echo 'End day date: '.$end_datetime."<br>\n";
	
	$endstamp = strtotime($end_datetime);
	return $endstamp;
}
