<?php
/**
 * Web Application Logger Stream class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Logger;

class Stream 
{
	protected $logger;
	protected $file;
			
    /**
     * Constructor
     */
	public function __construct(\Components\Logger $logger)
    {
		$this->logger = $logger;
	}
	
    /**
     * Has file opened
	 * @return bool
     */
    public function hasOpened()
    {
		return !is_null($this->file);
	}
	
    /**
     * Open file
	 * @return bool
     */
    public function open()
    {
		$dir = $this->logger->getDirectory();
		$path = $this->logger->getFilePath();
		
		if (!file_exists($dir)) {
			@mkdir($dir, 0777, true);
		}
		$this->file = fopen($path, 'a+b');
	}
	
    /**
     * Write data into file
	 * @param string $data
	 * @return bool
     */
    public function write($data)
    {
		if (!$this->hasOpened()) {
			$this->open();
		}
		if (flock($this->file, LOCK_EX)) {
			
			fwrite($this->file, $data);
			flock($this->file, LOCK_UN);
			
			if (mt_rand(0, 500) == 10) {
				$this->chekSize();
			}
			return true;
		}
		return false;
	}
	
    /**
     * Check file size
     */
    public function chekSize()
    {
		$stat = fstat($this->file);
		$max = $this->logger->maxSize();
		
		if ($stat['size'] > $max) {
			file_put_contents(
				$this->logger->getFilePath(), file_get_contents($this->logger->getFilePath(), false, null, $stat['size']-$max, $max), LOCK_EX
			);
		}
	}
	
    /**
     * Destructor
     */
    public function __destruct()
    {
		if ($this->hasOpened()) {
			$this->chekSize();
			fclose($this->file);	
		}
	}
}
