<?php
/**
 * Web Application Settngs class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;

abstract class Settings
{
	private static 
		$_settings = [];
	
    /**
     * Load settings by category
	 * @param string $category settings key category
     */
    private static function load($category)
    {
		if (!file_exists(CONFIG.'/'.$category.'.php')) {
			throw new \Exception('Error load settings: '.$category);
		}
		include CONFIG.'/'.$category.'.php';
		self::$_settings[$category] = $config;
	}
	
    /**
     * Get settings value
	 * @param string $key settings key
	 * @param string $default settings default
	 * @return mixed
     */
    public static function val($key = null, $default = null)
    {
        if (is_null($key)) {
            return self::$_settings;
        }
		if (!strpos($key, '.')) {
			$key = 'app.'.$key;
		}
		$keys = explode('.', $key);
		if (!isset(self::$_settings[$keys[0]])) {
			self::load($keys[0]);
		}
		if (empty($keys[1])) {
			return self::$_settings[$keys[0]];
		}
        if (!isset(self::$_settings[$keys[0]][$keys[1]])) {
            return $default;
        }
        return self::$_settings[$keys[0]][$keys[1]];
    }
}
