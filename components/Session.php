<?php
/**
 * Web Application Session class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Core\Traits,
	Components\Helpers\Hash;

class Session
{
	use Traits\Singleton;
	
	private static
		$_ready = false;
	private 
		$id,
		$wrapper,
		$cookie;
		
    /**
     * Constructor
     */
	private function __construct()
    {
		$wrapper = config('session.wrapper');
		$this->wrapper = new $wrapper($this);
	}
	
    /**
     * Register instance
     */
    public static function _register()
    {
		if (config('session.enable')) {
			static::getInstance()->start();
		}
	}
	
    /**
     * Start session
	 * @return bool
     */
    public function start()
    {
		if (static::$_ready) {
			throw new \Exception('Session already started');
		}
		static::$_ready = true;
		
		// механизм очистки от старых сессий
		if (mt_rand(1, 100) <= config('session.start_cleaner_chance')) {
			$this->clearOldSessions();
		}
		if ($cookie = $this->getCookie()) {
			// сессия существует
			$this->id = $cookie->id;
			if ($this->wrapper->exists() && $this->wrapper->loadContents()) {
				if (
					request()->clientIP() == $this->getProperty('ip_address') &&
					request()->userAgent() == $this->getProperty('user_agent') &&
					request()->acceptLang() == $this->getProperty('accept_lang')
				) {
					$this->wrapper->updateLastVisit(app()->startTime());
					if ($this->wrapper->getProperty('long')) {
						$this->updateCookie();
					}
					$this->trigger('continue');
					return true;
				}
			}
		}
		// сессия отсутствует
		$this->id = $this->getClientUniq();
		$this->setCookie();
		
        return $this->wrapper->create((object)[
			'ip_address' => request()->clientIP(),
			'user_agent' => request()->userAgent(),
			'accept_lang' => request()->acceptLang(),
			'last_visit' => app()->startTime(),
			'first_visit' => true,
			'long' => config('session.long'),
			'data' => []
		]);
		$this->trigger('start');
    }
	
    /**
     * Current session ID
	 * @return string
     */
    public function id()
    {
        return $this->id;
    }
	
    /**
     * Get current session ID
	 * @return string
     */
    public function isFirstVisit()
    {
        return (bool)$this->getProperty('first_visit');
    }

    /**
     * Has session key
	 * @return bool
     */
    public function has($key)
    {
		return $this->wrapper->has($key);
    }
	
    /**
     * Set session value
	 * @param string $key
	 * @param mixed $default
	 * @return bool
     */
    public function set($key, $val)
    {
		return $this->wrapper->set($key, $val);
    }
	
    /**
     * Get session value
	 * @param mixed $key
	 * @param mixed $default
	 * @return bool
     */
    public function get($key = null, $default = null)
    {
		return $this->wrapper->get($key, $default);
    }
	
    /**
     * Unset session key
	 * @param string $key
	 * @return bool
     */
    public function remove($key)
    {
		return $this->wrapper->remove($key);
    }
	
    /**
     * Unset session data
	 * @return bool
     */
    public function clear()
    {
		return $this->wrapper->clear();
    }
	
    /**
     * Destroy session
	 * @return void
     */
    public function destroy()
    {
		$this->wrapper->destroy();
		$this->id = null;
		$this->setCookie();
		$this->trigger('destroy');
    }
	
    /**
     * Get session value
	 * @param string $key
	 * @return bool
     */
    public function getProperty($key)
    {
		return $this->wrapper->getProperty($key);
    }
	
    /**
     * Has session property key
	 * @param string $key
	 * @return bool
     */
    public function hasProperty($key)
    {
		return $this->wrapper->hasProperty($key);
    }
	
    /**
     * Set session long status
	 * @param bool $status
	 * @return bool
     */
    public function setLongStatus($status)
    {
		$this->trigger('prolongate', [
			'session' => $this,
			'status' => $status
		]);
		return $this->wrapper->setProperty('long', (bool)$status);
    }
	
    /**
     * Update session cookie
	 * @param array $args
     * @return bool
     */
    public function updateCookie($args = [])
    {
		return $this->setCookie($args);
	}
	
   /**
     * Get client uniq
     * @return string
     * */
    private function getClientUniq()
    {
        $client = [
			app()->startTime(),
			request()->clientIP(),
			request()->userAgent(),
			request()->acceptLang(),
		];
		return md5(
			Hash::make($client)
		);
	}
	
    /**
     * Get session cookie
     * @return object
     */
    public function getCookie()
    {
		if (is_null($this->cookie)) {
			if (!$cookie_data = request()->cookie(config('session.cookie'))) {
				return false;
			}
			if (!$cookie = decrypt($cookie_data)) {
				return false;
			}
			$this->cookie = json_decode($cookie);
		}
		return $this->cookie;
	}
	
    /**
     * Set session cookie
	 * @param array $args
     * @return bool
     */
    private function setCookie($args = [])
    {
		$exists_data = [];
		if ($cookie_data = $this->getCookie()) {
			$exists_data = (array)$cookie_data;
		}
		$this->cookie = (object)array_merge(
			$exists_data, $args, ['id' => $this->id]
		);
		$expire = app()->getTime()+config('session.expiries_in');
		return setcookie(
			config('session.cookie'), encrypt(jsonEncode($this->cookie)), $expire, '/', '', config('session.secure'), true
		);
	}
	
	/**
	 * Clear old sessions
	 */
    public function clearOldSessions()
    {
		$this->trigger('clear.old');
		return $this->wrapper->clearOldSessions();
	}
	
    /**
     * Session event trigger
	 * @param string $event
	 * @param array $args
     */
	public function trigger($event, $args = [])
	{
		$args['session'] = $this;
		return events()->trigger('session.'.$event, $args);
	}
}
