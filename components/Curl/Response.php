<?php
/**
 * Curl response class
 */
namespace Components\Curl;

class Response
{
	private 
		$data,
		$resource;
	
    /**
     * Constructor
	 * @param string $data - response content
	 * @param string $resource - curl resource
     */
    public function __construct($data, $resource)
    {
		$this->data = $data;
		$this->resource = $resource;
    }
	
    /**
     * Get response content
	 * @return string
     */
	public function getData()
	{
		return $this->data;
	}
	
    /**
     * Get json decoded content
	 * @return mixed|null
     */
	public function parseJson()
	{
		return json_decode($this->data);
	}
	
    /**
     * Get response code
	 * @return object
     */
	public function getInfo()
	{
		return (object)curl_getinfo($this->resource);
	}
	
    /**
     * Get response code
	 * @return integer
     */
	public function getCode()
	{
		return curl_getinfo($this->resource, CURLINFO_HTTP_CODE);
	}
	
    /**
     * Get curl error
	 * @return string
     */
	public function getError()
	{
		return curl_error($this->resource);
	}
}
