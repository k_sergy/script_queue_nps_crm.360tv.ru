<?php
/**
 * Web Application Logger class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Components\Support\Str;

class Logger 
{
	private $FM,
			$path = '/resources/logs/',
			$options = [
				'name' => '',
				'chunk' => 'm-Y',
				'date_format' => "d.m.Y H:i:s \r\n-------------------\r\n",
				'max_size' => 33554432
			],
			$stream;
	private static
			$_instances = [];
			
    /**
     * Constructor
     */
	private function __construct($name)
    {
		if (!is_string($name) && !is_array($name)) {
			throw new \Exception('Logger filename must be string or array');
		}
		if (is_string($name)) {
			$name = ['name' => $name];
		}
		$this->options = array_merge($this->options, $name);
		$this->options['name'] = ltrim($this->options['name'], '/');
		
		if (strpos($this->options['name'], '/') > 0) {
			$exps = explode('/', $this->options['name']);
			$this->options['name'] = array_pop($exps);
			$this->path = '/resources/logs/'.join('/', $exps).'/';
		}
		if ($this->options['chunk'] != '') {
			$this->path .= date($this->options['chunk'].'/', getTime());
		}
		$this->stream = new Logger\Stream($this);
	}
	
    /**
     * Get instance
	 * @return Logger
     */
    public static function getInstance($name = null)
    {
		if (is_null($name)) {
			if (empty(static::$_instances)) {
				throw new \Exception('Empty logger instance');
			}
			return end(static::$_instances);
		}
		if (!array_key_exists($name, static::$_instances)) {
			static::$_instances[$name] = new static($name);
		}
		return static::$_instances[$name];
	}
	
    /**
     * Set/get max log file size
	 * @param integer|null $mb
	 * @return integer
     */
    public function maxSize($mb = null)
    {
		if (is_int($mb)) {
			$this->options['max_size'] = $mb*1024*1024;
		}
		return $this->options['max_size'];
	}
	
    /**
     * Get logger dir path
	 * @return string
     */
    public function getDirectory()
    {
		return ROOT.$this->path;
	}
	
    /**
     * Get logger file path
	 * @return string
     */
    public function getFilePath()
    {
		return ROOT.$this->path.$this->options['name'];
	}
	
    /**
     * Log data
	 * @param mixed ... args
	 * @return bool
     */
    public function log()
    {
		$write = [];
		$args = func_get_args();
		
		foreach ($args as $arg) {
			$write[]= Str::toString($arg);
		}
		return $this->stream->write(
			date($this->options['date_format'], getTime())." - \t".join(", \r\n - \t", $write)."\r\n \r\n"
		);
	}
}
