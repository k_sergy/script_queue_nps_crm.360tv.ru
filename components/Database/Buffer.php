<?php
/**
 * Web Application DB buffer
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Database;

abstract class Buffer
{
    protected static $_contents = [];

    /**
     * Has bufer value 
	 * @param string $query - sql query hash
	 * @return bool
     */
    public static function has($query)
    {
		return array_key_exists($query, static::$_contents);
    }
	
    /**
     * Set bufer value 
	 * @param string $query - sql query hash
	 * @param mixed $content - sql result
	 * @return mixed
     */
    public static function set($query, $content)
    {
        static::$_contents[$query] = $content;
		return $content;
    }

    /**
     * Get buffer value
	 * @param string $query - sql query hash
	 * @return mixed
     */
    public static function get($query)
    {
		if (!array_key_exists($query, static::$_contents)) {
			return null;
		}
        return static::$_contents[$query];
    }
	
    /**
     * Remove buffer value
	 * @param string $query - sql query hash
	 * @return bool
     */
    public static function remove($query)
    {
		if (array_key_exists($query, static::$_contents)) {
			unset(static::$_contents[$query]);
			return true;
		}
		return false;
    }
}
