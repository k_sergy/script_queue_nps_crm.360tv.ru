<?php
/**
 * Email tpl class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Email;

class Template
{
	private 
		$path,
		$email,
		$vars = [];
		
    /**
     * Constructor
     * @param Email $email email object
     */
    public function __construct(\Components\Email $email)
    {
		$this->path = RESOURCES.'/templates/email';
		$this->email = $email;
	}
	
    /**
     * Set template
	 * @param string $name template name
	 * @return EmailTpl
     */
	public function load($name = 'default')
	{
		$name = preg_replace('#[^a-zA-Z0-9_\-]+#Uis', '', (string)$name);
		if (!file_exists($this->path.'/'.$name.'.html')) {
			throw new \Exception('Error mail tpl: '.$name);
		}
		if (!$html = file_get_contents($this->path.'/'.$name.'.html')) {
			throw new \Exception('Error load mail tpl: '.$name);
		}
		$this->email->body($html);
		return $this;
	}
	
    /**
     * Set replace variable
	 * @param string $name template variable name
	 * @param string $value template variable value
	 * @return EmailTpl
     */
	public function set($name, $value = '')
	{
		if (is_array($value)) {
			foreach ($value as $key=>$val) {
				$this->set($name.'_'.$key, $val);
			}
			return $this;
		}
		$this->vars['{'.$name.'}'] = $value;
		return $this;
	}
	
    /**
     * Build template
	 * @return EmailTpl
     */
	public function build()
	{
		$this->email->subject(strtr($this->email->subject, $this->vars));
		$this->set('subject', $this->email->subject);
		$this->email->body(strtr($this->email->body, $this->vars));
		return $this;
	}	
}
