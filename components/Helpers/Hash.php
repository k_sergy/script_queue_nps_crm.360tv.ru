<?php
/**
 * Hash operations
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Helpers;
use Components\Support\Str;

abstract class Hash
{
	/**
	 * Create hash from string
	 * @param string $from
	 * @return string
	 */
    public static function make($from)
    {
		return password_hash(Str::toString($from), PASSWORD_DEFAULT);
	}
	
	/**
	 * Check hash from string
	 * @param string $from
	 * @param string $hash
	 * @return bool
	 */
    public static function check($from, $hash)
    {
		return password_verify($from, $hash);
	}
}
