<?php
/**
 * Web Application Queue class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Core\Traits;
use Components\Database\Connection as Db;

class Queue
{
	use Traits\Singleton;
	const ATTEMPTS = 2;
	
	protected $que_job_class = \Core\Models\QueueJob::class;
	protected $delay_step = 30;

    /**
     * Constructor
     */
    protected function __construct() {}
	
	/**
	 * Get job
	 * @param integer $job_id
	 * @param Job $job
	 */
    public function find($job_id)
    {
		$jobClass = $this->que_job_class;
		Db::link()->setQueryOption('SQL_NO_CACHE');
		return $jobClass::find($job_id);
	}
	
	/**
	 * Push new
	 * @param Job $job
	 * @param string $name
	 * @param mixed $aviable_At
	 * @return QueueJob
	 */
    public function push(\Core\Jobs\Job $job, $name = 'default', $private = 0)
    {
		$jobClass = $this->que_job_class;
		$queueJob = $jobClass::create([
			'name' => $name,
			'private' => $private,
			'state' => -2
		]);
		$job->setQueue($queueJob);
		$queueJob->setJob($job);
		return $queueJob;
	}
	
	/**
	 * Check crashed jobs and repair
	 * @return Collection
	 */
    public function checkJobs()
    {
		$jobClass = $this->que_job_class;
		Db::link()->setQueryOption('SQL_NO_CACHE');
		$qjobs = $jobClass::get([
			'state' => 2, 
			'modified_at' => [dateTime()->sub(new \DateInterval('PT300S'))->format('Y-m-d H:i:s'), '<']
		]);
		$qjobs->update([
			'state' => 0
		]);
		return $qjobs;
	}
	
	/**
	 * Get current runned jobs count by min thread
	 * @param integer $min_thread
	 * @return array
	 */
    public function getOvers($min_thread = 1)
    {
		$jobClass = $this->que_job_class;
		$jobs = [];
		$selected = Db::link()
			->setQueryOption('SQL_NO_CACHE')
			->setQueryOption('SQL_SMALL_RESULT')
			->fromTable($jobClass::getTableName())
			->where('state', 2)
			->groupBy('name')
			->having('count', $min_thread, '>=')
			->get(null, 'COUNT(id) as count, name');
		foreach ($selected as $row) {
			$jobs[$row['name']] = $row['count'];
		}
		return $jobs;
	}
	
	/**
	 * Get aviable jobs
	 * @param array $disable_names
	 * @param integer $limit max count
	 * @return QueueJob
	 */
    public function getAviableJobs(array $disable_names = [], $limit = 10)
    {
		$jobClass = $this->que_job_class;
		if (!empty($disable_names)) {
			Db::link()->where('name', $disable_names, 'NOT IN');
		}
		$jobs = collection([]);
		$selected = Db::link()->setQueryOption('SQL_NO_CACHE')
			  ->fromTable($jobClass::getTableName())
			  ->where('state', 0)
			  ->where('private', 0)
			  ->where('completed_at', null, 'IS')
			  ->where('(aviable_at IS NULL OR aviable_at <= ?)', [dateFor('Y-m-d H:i:s')])
			  ->orderBy('id', 'ASC')
			  ->groupBy('name')
			  ->get($limit, 'id, name');
		if ($selected) {
			foreach ($selected as $job) {
				$jobs->push(
					new $jobClass($job)
				);
			}
		}
		return $jobs;
	}
	
    public function getAviableJobsDebug($name = null, $limit = 10)
    {
		$jobClass = $this->que_job_class;
		if (!empty($disable_names)) {
			Db::link()->where('name', $disable_names, 'NOT IN');
		}
		$jobs = collection([]);
		Db::link()->setTrace(1);
		$selected = Db::link()->setQueryOption('SQL_NO_CACHE')
			  ->fromTable($jobClass::getTableName())
			  ->where('state', 0)
			  ->where('private', 0)
			  ->where('completed_at', null, 'IS')
			  ->where('(aviable_at IS NULL OR aviable_at <= ?)', [dateFor('Y-m-d H:i:s')])
			  ->orderBy('id', 'ASC')
			  ->groupBy('name')
			  ->get($limit, 'id, name');
		print_r(Db::link()->trace);
		if ($selected) {
			foreach ($selected as $job) {
				$jobs->push(
					new $jobClass($job)
				);
			}
		}
		return $jobs;
	}
	
	/**
	 * Get current jobs
	 * @return array
	 */
    public function getCurrentJobs()
    {
		$jobClass = $this->que_job_class;
		$jobs = [];
		Db::link()->setTrace(1);
		$selected = Db::link()->setQueryOption('SQL_NO_CACHE')
			->fromTable($jobClass::getTableName())
			->where('state', 2)
			->groupBy('name')
			->get(null, 'COUNT(id) as count, name');
		foreach ($selected as $row) {
			$jobs[$row['name']] = $row['count'];
		}
		return $jobs;
	}
	
	/**
	 * Get waiting jobs
	 * @return array
	 */
    public function getWaitJobs()
    {
		$jobClass = $this->que_job_class;
		$jobs = [];
		Db::link()->setTrace(1);
		$selected = Db::link()->setQueryOption('SQL_NO_CACHE')
			->fromTable($jobClass::getTableName())
			->where('state', 0)
			->where('private', 0)
			->where('completed_at', null, 'IS')
			->where('(aviable_at IS NULL OR aviable_at <= ?)', [dateFor('Y-m-d H:i:s')])
			->groupBy('name')
			->get(null, 'COUNT(name) as count, name');
		foreach ($selected as $row) {
			$jobs[$row['name']] = $row['count'];
		}
		return $jobs;
	}
	
	/**
	 * Process job
	 * @param QueueJob $queueJob
	 * @param bool $force
	 */
    public function process(&$queueJob, $force = false)
    {
		$jobClass = $this->que_job_class;
		if (!$queueJob instanceof $jobClass) {
			throw new \Exception('QueueJob must be instance of: \Core\Models\QueueJob');
		}
		if ($queueJob->hasCompleted()) {
			throw new \Exception('Job is completed in: '.$queueJob->completed_at);
		}
		if (!$queueJob->hasAviableNow() && !$force) {
			$queueJob->setState(0);
			return false;
		}
		try {
			$job = $queueJob->getJob();
		} catch(\Exception $e) {
			$queueJob->update(['completed_at' => dateFor('Y-m-d H:i:s')]);
			throw new \Exception('Job load fail: '.$queueJob->id);
		}
		$queueJob->attempt++;
		$queueJob->update(['state' => 2, 'attempt' => $queueJob->attempt]);
		$stock = static::ATTEMPTS-$queueJob->attempt;
		
		if ($stock >= 0) {
			try {
				call_user_func_array(
					[$job, 'handle'], getMethodRegisteredArgs($job, 'handle')
				);
				$queueJob->update(['state' => 1, 'completed_at' => dateFor('Y-m-d H:i:s')]);
				$queueJob->removeException();
			} catch(\Exception $e) {
				$job->setException($e);
				//$queueJob->setJob($job);
				//error_log('Queue job '.$queueJob->id.' error: '.$e->getMessage());
				if ($stock == 0) {
					$queueJob->update(['state' => -1, 'completed_at' => dateFor('Y-m-d H:i:s')]);
				} else {
					$queueJob->setState(0);
					$queueJob->delay(
						$this->getDelay($queueJob)
					);
				}
			}
		} else {
			$queueJob->update(['state' => -1, 'completed_at' => dateFor('Y-m-d H:i:s')]);
		}
		return true;
	}
	
	/**
	 * Get queue job delay
	 * @param QueueJob $queueJob
	 * @return integer
	 */
    public function getDelay($queueJob)
    {
		$jobClass = $this->que_job_class;
		if (!$queueJob instanceof $jobClass) {
			throw new \Exception('QueueJob must be instance of: \Core\Models\QueueJob');
		}
		$mn = $queueJob->attempt > 0 ? $queueJob->attempt : 1;
		return $mn*$this->delay_step;
	}
}
