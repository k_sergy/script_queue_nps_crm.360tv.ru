<?php
/**
 * Web Application Session wrapper class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Session\Wrappers;

class Files
{
	private 
		$FM,
		$contents,
		$session,
		$suff = '.sess';
		
    /**
     * Constructor
     */
    public function __construct(\Components\Session $session)
    {
		$this->session = $session;
		$this->FM = fileManager('/app/Storage/Sessions/');
	}
	
    /**
     * Get session filename
	 * @return string
     */
    private function sessionFile()
    {
		return $this->session->id().$this->suff;
	}
	
    /**
     * Create new session
	 * @return bool
     */
    public function create($contents)
    {
		$this->contents = $contents;
		return $this->FM->createFile(
			$this->sessionFile(), encrypt($this->contents)
		);
    }
	
    /**
     * Set session last visit
	 * @return bool
     */
    public function updateLastVisit($time)
    {
		$this->contents->last_visit = $time;
		$this->contents->first_visit = false;
		return $this->saveContents();
    }
	
    /**
     * Is session exists
	 * @return bool
     */
    public function exists()
    {
		return $this->FM->exists($this->sessionFile());
    }
	
    /**
     * Load session contents
	 * @return bool
     */
    public function loadContents()
    {
		return $this->contents = decrypt(
			$this->FM->get($this->sessionFile())
		);
    }
	
    /**
     * Has session property
	 * @param string $key
	 * @return bool
     */
    public function hasProperty($key)
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		if (!property_exists($this->contents, $key)) {
			return false;
		}
		return true;
    }
	
    /**
     * Get session property
	 * @param string $key
	 * @return bool
     */
    public function getProperty($key)
    {
		if (!$this->hasProperty($key)) {
			throw new \Exception('Invalid session property: '.$key);
		}
		if (in_array($key, ['data'])) {
			throw new \Exception('Error read session system property: data');
		}
		return $this->contents->$key;
    }
	
    /**
     * Set session property
	 * @param string $key
	 * @return bool
     */
    public function setProperty($key, $data)
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		if (!property_exists($this->contents, $key)) {
			throw new \Exception('Invalid session property: '.$key);
		}
		if (in_array($key, ['data'])) {
			throw new \Exception('Error write to session system property: data');
		}
		$this->contents->$key = $data;
		return $this->saveContents();
    }
	
    /**
     * Set session contents
	 * @return bool
     */
    private function saveContents()
    {
		return $this->FM->put(
			$this->sessionFile(), encrypt($this->contents)
		);
    }
	
    /**
     * Has session key
	 * @param string $key
	 * @return bool
     */
    public function has($key)
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		return array_key_exists($key, $this->contents->data);
    }
	
    /**
     * Set session value
	 * @param string $key
	 * @param mixed $default
	 * @return bool
     */
    public function set($key, $val)
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		$this->contents->data[$key] = $val;
		return $this->saveContents();
    }
	
    /**
     * Get session value
	 * @param mixed $key
	 * @param mixed $default
	 * @return bool
     */
    public function get($key = null, $default = null)
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		if (is_null($key)) {
			return $this->contents->data;
		}
		if (!array_key_exists($key, $this->contents->data)) {
			return $default;
		}
		return $this->contents->data[$key];
    }
	
    /**
     * Unset session key
	 * @param string $key
	 * @return bool
     */
    public function remove($key)
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		if (!array_key_exists($key, $this->contents->data)) {
			return false;
		}
		unset($this->contents->data[$key]);
		return $this->saveContents();
    }
	
    /**
     * Unset session data
	 * @return bool
     */
    public function clear()
    {
		if (!is_object($this->contents)) {
			throw new \Exception('Session not started');
		}
		$this->contents->data = [];
		return $this->saveContents();
    }
	
    /**
     * Destroy session
	 * @return bool
     */
    public function destroy()
    {
		return $this->FM->remove(
			$this->sessionFile()
		);
    }
	
	/**
	 * Clear old sessions
	 */
    public function clearOldSessions()
    {
		if (exec_enabled()) {
			return $this->clearOldSessions_WithCmd();
		}
		return $this->clearOldSessions_WithFileManager();
	}
	
	/**
	 * Clear old sessions with cmd
	 */
    private function clearOldSessions_WithCmd()
    {
		$sessions = [];
		$cmd = 'find '.STORAGE.'/Sessions -type f -name "*.sess" -mmin +'.config('session.expiries_in')/60;
		exec($cmd, $sessions, $ret);
		
		if ($ret === 0) {
			foreach ($sessions as $session_file_path) {
				@unlink($session_file_path);
			}
		}
	}
	
	/**
	 * Clear old sessions with FileManager
	 */
    private function clearOldSessions_WithFileManager()
    {
		$fm = $this->FM;
		$sessions = $fm->search('*.sess')->filter(function($file) use($fm) {
			return (getTime()-$fm->getModified($file)) > config('session.expiries_in');
		});
		$sessions->each(function($file) use($fm) {
			$fm->remove($file);
		});
	}
}
