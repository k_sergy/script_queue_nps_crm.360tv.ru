# README #

MVC Platform [wiki-page](https://bitbucket.org/cmdf5team/mvc-platform/wiki/browse/)

1) [Установка](https://bitbucket.org/cmdf5team/mvc-platform/wiki/%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0)

2) [Общая настройка](https://bitbucket.org/cmdf5team/mvc-platform/wiki/%D0%9E%D0%B1%D1%89%D0%B8%D0%B5%20%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B8)

3) [Правила роутера](https://bitbucket.org/cmdf5team/mvc-platform/wiki/%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0%20%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%20%D1%80%D0%BE%D1%83%D1%82%D0%B5%D1%80%D0%B0)

4) [Регистрация моделей](https://bitbucket.org/cmdf5team/mvc-platform/wiki/%D0%A0%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D1%8F%20%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D0%B5%D0%B9)

5) [Создание и обработка событий](https://bitbucket.org/cmdf5team/mvc-platform/wiki/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%B8%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0%20%D1%81%D0%BE%D0%B1%D1%8B%D1%82%D0%B8%D0%B9)

