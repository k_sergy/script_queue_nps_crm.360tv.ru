<?=$this->content('header', $data);?>
<?=$this->content('navbar', $data);?>

    <div class="container">
      <div class="mt-3">
        <h1>MVC Platform</h1>
      </div>
      <p><a href="https://bitbucket.org/cmdf5team/mvc-platform/wiki/browse/" target="_blank">Wiki-страница</a> репозитория.</p>
    </div>
	
<?=$this->content('footer', $data);?>