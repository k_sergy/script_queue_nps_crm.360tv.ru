-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 26 2018 г., 13:02
-- Версия сервера: 10.2.7-MariaDB
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mvcp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `model_company`
--

CREATE TABLE `model_company` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `model_role`
--

CREATE TABLE `model_role` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `model_user`
--

CREATE TABLE `model_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `company_id` int(11) UNSIGNED DEFAULT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `queue_job`
--

CREATE TABLE `queue_job` (
  `id` int(10) UNSIGNED NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT 'default',
  `attempt` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `completed_at` datetime DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `aviable_at` datetime DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_to_role`
--

CREATE TABLE `user_to_role` (
  `link_id` varchar(32) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `model_company`
--
ALTER TABLE `model_company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `model_role`
--
ALTER TABLE `model_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `model_user`
--
ALTER TABLE `model_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD KEY `user_to_company` (`company_id`);

--
-- Индексы таблицы `queue_job`
--
ALTER TABLE `queue_job`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state` (`state`,`aviable_at`),
  ADD KEY `priority` (`priority`),
  ADD KEY `name` (`name`),
  ADD KEY `private` (`private`),
  ADD KEY `completed_at` (`completed_at`);

--
-- Индексы таблицы `user_to_role`
--
ALTER TABLE `user_to_role`
  ADD PRIMARY KEY (`link_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`role_id`),
  ADD KEY `user_to_role` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `model_company`
--
ALTER TABLE `model_company`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT для таблицы `model_role`
--
ALTER TABLE `model_role`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT для таблицы `model_user`
--
ALTER TABLE `model_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT для таблицы `queue_job`
--
ALTER TABLE `queue_job`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `model_user`
--
ALTER TABLE `model_user`
  ADD CONSTRAINT `user_to_company` FOREIGN KEY (`company_id`) REFERENCES `model_company` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user_to_role`
--
ALTER TABLE `user_to_role`
  ADD CONSTRAINT `role_to_user` FOREIGN KEY (`user_id`) REFERENCES `model_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_to_role` FOREIGN KEY (`role_id`) REFERENCES `model_role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
