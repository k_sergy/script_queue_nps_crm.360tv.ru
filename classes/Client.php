<?php

namespace Classes;

use Ufee\Amo\Amoapi;

class Client
{
    use \Core\Traits\Singleton;
    private static $_crm;

    /**
     * Get amoCRM API client
     * @return CRM
     */
    public function crm()
    {
        if (is_null(static::$_crm)) {
            static::$_crm = Amoapi::setInstance(config('crm.'));
            static::$_crm->autoAuth(true);
        }
        return static::$_crm;
    }
}