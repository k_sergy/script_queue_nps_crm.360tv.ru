<?php
/**
 * Web Application config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
	// Предполагаемый заголовок сайта
	'apptitle' => 'MVC Platform',
	
	// Локаль в PHP (по умолч. ru_RU.UTF-8)
	'locale' => 'ru_RU.UTF-8',
	// Часовой пояс (по умолч. МСК)
	'timezone' => 'Europe/Moscow',
	// Часовой пояс (по умолч. МСК)
	'hours_offset' => 0, # 1,-2
	
	'appuri' => '/cmdf5', # /example for dir
	'hosturl' => 'http://crm.360tv.ru',
	'real_path_offset' => '/cmdf5', # /example for dir

	// буферизация вывода в PHP (по умолч. вкл)
	'output_buffer' => 1,
	// Минификация HTML кода, отдаваемого вьюхами (по умолч. выкл)
	'output_minify' => 0,
	
	// Уникальный ключ приложения (22 символа, заменить обязательно!)
	'key' => '!6&u7dr$g2d236uj1@349f',
];
