<?php
/**
 * Web Application DB config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
	'default' => [
		'host' => 'localhost',
		'username' => 'phpmyadmin',
		'password' => '360amocrm#php',
		'db' => 'amocrm_cmdf5',
		'port' => 3306,
		'prefix' => '',
		'charset' => 'utf8'
	],
	'other' => [
		'host' => 'localhost',
		'username' => '',
		'password' => '',
		'db' => '',
		'port' => 3306,
		'prefix' => '',
		'charset' => 'utf8'
	]
];