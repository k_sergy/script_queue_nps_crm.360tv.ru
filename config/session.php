<?php
/**
 * Web Application sessions config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
	// Включение сессий (по умолч. вкл.)
	'enable' => false,
	// Обработчик сессий (по умолч. файлы)
	'wrapper' => Components\Session\Wrappers\Files::class,
	// Название Cookie сессии 
	'cookie' => 'MVCP_SESS',
	// Время жизни сессий, сек
	'expiries_in' => 3600*24*1,
	// Шанс запуска очистки, ~1 из 100 = 3
	'start_cleaner_chance' => 3,
	// Передача cookie по https
	'secure' => false,
	// Автопродление сессии
	'long' => false,
];
