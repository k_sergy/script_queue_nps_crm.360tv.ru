<?php
/**
 * Web Application Controller pages
 */
namespace App\Controllers;
use Core\Application,
	Components\Request,
	Components\Logger,
	App\Jobs\SetFieldLead,
	Components\Queue;

class AmoWebhook
{
    public function index(Request $request)
    {	
    	$l = logger('/hook/logs.log');

    	$webhook = request()->input();
    	$l->log($webhook);
		if(isset($webhook['leads'])) {
			$hook = $webhook['leads']['status'][0];
			events()->trigger('hook.event', [
				'hook' => $hook
			]);
		}

		return true;
		
	}
}
