<?php
/**
 * Web Application Controller pages
 */
namespace App\Controllers;
use Core\Application,
	App\Models\Company,
	App\Models\User,
	App\Models\Role,
	Components\Request,
	Components\Response,
	Components\Security\Crypto,
	Components\Sessions\Session,
	Components\Helpers\Hash,
	Components\Queue;

class Page extends \Core\Controllers\Controller
{
    public function __construct()
    {
        parent::__construct();
		
		$this->middleware('AllowOrigin');
	}
	
	/**
	 * Test 
	 */
    public function test(Request $request, Application $app, Queue $queue)
    {
		$current_jobs = $queue->getCurrentJobs();
		echo 'Current jobs runned: ';
		print_r($current_jobs);
		
		$wait_jobs = $queue->getWaitJobs();
		echo 'Waiting jobs: ';
		print_r($wait_jobs);
		
		$overs = $queue->getOvers(1);
		echo 'Overs: ';
		print_r($overs);
		
		$availableJobs = $queue->getAviableJobsDebug(array_keys($overs));
		echo 'Available jobs: ';
		print_r($availableJobs->all());

/* 		$job = new \App\Jobs\SomeJob;
		$qjob = $queue->push($job);
		$qjob->setState(0);
		print_r($qjob);
		print_r($job); */
		
	}
}
