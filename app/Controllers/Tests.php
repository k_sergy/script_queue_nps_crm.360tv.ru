<?php
/**
 * Web Application Controller
 */
namespace App\Controllers;
use Components\Request,
	App\Models\Company,
	App\Models\User,
	App\Models\Role;
use Classes\Client;

class Tests extends \Core\Controllers\Controller
{
	private $tests = [
			'Company' => 'Company',
			'Users' => 'Users',
			'CompanyUsers' => 'Company -> Users',
			'UserCompany' => 'User -> Company',
			'UserRoles' => 'User -> Roles',
			'OpenSSL' => 'OpenSSL -> Encrypt/Decrypt'
		];
		
	/**
	 * Index page
	 */
    public function index(Request $request)
    {

    	$this->client = app('client');
    	$lead = $this->client->crm()->leads()->find(25594643);
    	print_r( $lead );
    	
    	die;

		if ($request->input('run')) {
			return $this->{$request->input('run')}();
		}
		view('main.tests/index', [
			'title' => 'Tests',
			'tests' => $this->tests
		]);
	}
	
    private function Company()
    {
		$lines = [];
		$lines[]= 'Удаление всех компаний';
		Company::removeAll();
		$companys = Company::get();
		if ($companys->count() == 0) {
			$lines[]= '<span class="text-success">Компаний: '.$companys->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Компаний: '.$companys->count().'</span>';
		}
		$lines[]= 'Создание компании: Команда F5';
		$company = Company::create([
			'name' => 'Команда F5',
		]);
		if (is_object($company)) {
			$lines[]= '<span class="text-success">Создано</span>';
			$lines[]= '<span class="text-success">'.print_r($company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Не создано</span>';
		}
		$lines[]= 'Получение компаний';
		$companys = Company::get();
		if ($companys->count() == 1) {
			$lines[]= '<span class="text-success">Получено: '.$companys->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Получено: '.$companys->count().'</span>';
		}
		if ($company->name == 'Команда F5') {
			$lines[]= '<span class="text-success">'.print_r($company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($company->toArray(), 1).'</span>';
		}
		$lines[]= 'Переименование компании в: ООО Команда F5';
		$company->name = 'ООО Команда F5';
		$company->save();
		$lines[]= 'Получение компании по ID';
		$company = Company::find($company->id);
		if (is_object($company)) {
			$lines[]= '<span class="text-success">Получено</span>';
		} else {
			$lines[]= '<span class="text-danger">Не получено</span>';
		}
		if ($company->name == 'ООО Команда F5') {
			$lines[]= '<span class="text-success">'.print_r($company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($company->toArray(), 1).'</span>';
		}
		$lines[]= 'Удаление компании';
		$company->remove();
		$companys = Company::get();
		if ($companys->count() == 0) {
			$lines[]= '<span class="text-success">Компаний: '.$companys->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Компаний: '.$companys->count().'</span>';
		}
		$lines[]= 'Конец';
		view('main.tests/test', [
			'title' => $this->request->input('run'),
			'lines' => $lines,
		]);
	}
	
    private function Users()
    {
		$lines = [];
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Создание юзера: Жорик, test@f5.com.ru, 89001';
		$user = User::create([
			'name' => 'Жорик',
			'login' => 'test@f5.com.ru',
			'phone' => '89001',
		]);
		if (is_object($user)) {
			$lines[]= '<span class="text-success">Создано</span>';
		} else {
			$lines[]= '<span class="text-danger">Не создано</span>';
		}
		$lines[]= 'Получение юзеров';
		$users = User::get();
		if ($users->count() == 1) {
			$lines[]= '<span class="text-success">Получено: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Получено: '.$users->count().'</span>';
		}
		if ($user->name == 'Жорик') {
			$lines[]= '<span class="text-success">'.print_r($user->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($user->toArray(), 1).'</span>';
		}
		$lines[]= 'Переименование юзера в: Жора Журавлев';
		$user->name = 'Жора Журавлев';
		$user->save();
		$lines[]= 'Получение юзера по ID';
		$user = User::find($user->id);
		if (is_object($user)) {
			$lines[]= '<span class="text-success">Получено</span>';
		} else {
			$lines[]= '<span class="text-danger">Не получено</span>';
		}
		if ($user->name == 'Жора Журавлев') {
			$lines[]= '<span class="text-success">'.print_r($user->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($user->toArray(), 1).'</span>';
		}
		$lines[]= 'Задаем пароль юзера: 1234';
		$user->setPassword(1234);
		$user->save();
		$user = User::find($user->id);
		$lines[]= 'Проверяем пароль юзера: 1234';
		if ($user->hasPassword(1234)) {
			$lines[]= '<span class="text-success">Пароль верный</span>';
		} else {
			$lines[]= '<span class="text-danger">Пароль НЕ верный</span>';
		}
		$lines[]= 'Задаем пароль юзера: ab$v%y/91x';
		$user->setPassword('ab$v%y/91x');
		$user->save();
		$user = User::find($user->id);
		$lines[]= 'Проверяем пароль юзера: ab$v%y/91x';
		if ($user->hasPassword('ab$v%y/91x')) {
			$lines[]= '<span class="text-success">Пароль верный</span>';
		} else {
			$lines[]= '<span class="text-danger">Пароль НЕ верный</span>';
		}
		$lines[]= 'Проверяем пароль юзера: 1234';
		if (!$user->hasPassword('1234')) {
			$lines[]= '<span class="text-success">Пароль НЕ верный</span>';
		} else {
			$lines[]= '<span class="text-danger">Пароль верный</span>';
		}
		$lines[]= 'Создание юзера: Егор, egor@f5.com.ru, 89001';
		$egor = User::create([
			'name' => 'Егор',
			'login' => 'egor@f5.com.ru',
			'phone' => '89001',
		]);
		if ($egor->phone == '89001') {
			$lines[]= '<span class="text-success">'.print_r($egor->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($egor->toArray(), 1).'</span>';
		}
		$lines[]= 'Получаем юзеров по телефону: 89001';
		$users = User::get([
			'phone' => 89001,
		]);
		if ($users->count() == 2) {
			$lines[]= '<span class="text-success">Получено: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Получено: '.$users->count().'</span>';
		}
		$lines[]= 'Удаление Егора';
		$egor->remove();
		$users = User::get();
		if ($users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Конец';
		view('main.tests/test', [
			'title' => $this->request->input('run'),
			'lines' => $lines,
		]);
	}

    private function CompanyUsers()
    {
		$lines = [];
		$lines[]= 'Удаление всех компаний';
		Company::removeAll();
		$companys = Company::get();
		if ($companys->count() == 0) {
			$lines[]= '<span class="text-success">Компаний: '.$companys->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Компаний: '.$companys->count().'</span>';
		}
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Создание компании: Команда F5';
		$company = Company::create([
			'name' => 'Команда F5',
		]);
		if (is_object($company)) {
			$lines[]= '<span class="text-success">'.print_r($company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($company->toArray(), 1).'</span>';
		}
		$lines[]= 'Проверка кол-ва юзеров компании';
		if ($company->users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Создание юзера: Жорик, test@f5.com.ru';
		$jora = User::create([
			'name' => 'Жорик',
			'login' => 'test@f5.com.ru',
		]);
		if (is_object($jora)) {
			$lines[]= '<span class="text-success">'.print_r($jora->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($jora->toArray(), 1).'</span>';
		}
		$lines[]= 'Прикрепление Жорика к компании';
		$company->users()->attach($jora);
		if ($company->users->count() === 1) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
			$lines[]= '<span class="text-success">'.print_r($company->users->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Проверка Жорика на наличие компании';
		if (is_object($jora->company) && $jora->company->id == $company->id) {
			$lines[]= '<span class="text-success">'.print_r($jora->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($jora->toArray(), 1).'</span>';
		}
		$lines[]= 'Создание юзера: Василий, vass@f5.com.ru';
		$vass = User::create([
			'name' => 'Василий',
			'login' => 'vass@f5.com.ru',
		]);
		if (is_object($vass)) {
			$lines[]= '<span class="text-success">'.print_r($vass->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($vass->toArray(), 1).'</span>';
		}
		$lines[]= 'Прикрепление Василия к компании';
		$company->users()->attach($vass);
		if ($company->users->count() == 2) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
			foreach ($company->users as $user) {
				$lines[]= '<span class="text-info">Юзер: '.$user->name.'</span>';
			}
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Проверка Василия на наличие компании';
		if (is_object($vass->company) && $vass->company->id == $company->id) {
			$lines[]= '<span class="text-success">'.print_r($vass->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($vass->toArray(), 1).'</span>';
		}
		$lines[]= 'Открепление Василия от компании';
		$company->users()->detach($vass);
		if ($company->users->count() === 1) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
			$lines[]= '<span class="text-success">'.print_r($company->users->last()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Проверка Василия на наличие компании';
		if (is_object($vass->company) && $vass->company->id == $company->id) {
			$lines[]= '<span class="text-danger">'.print_r($vass->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-success">'.print_r($vass->toArray(), 1).'</span>';
			$vass->remove();
		}
		$lines[]= 'Создание юзера через компанию: Егор, egor@f5.com.ru';
		$egor = $company->users()->create([
			'name' => 'Егор',
			'login' => 'egor@f5.com.ru',
		]);
		if (is_object($egor)) {
			$lines[]= '<span class="text-success">'.print_r($egor->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($egor->toArray(), 1).'</span>';
		}
		$lines[]= 'Проверка кол-ва юзеров компании';
		if ($company->users->count() == 2) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
			$lines[]= '<span class="text-success">'.print_r($company->users->last()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Проверка Егора на наличие компании';
		if (is_object($egor->company) && $egor->company->id == $company->id) {
			$lines[]= '<span class="text-success">'.print_r($egor->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($egor->toArray(), 1).'</span>';
		}
		$lines[]= 'Удаление Егора через компанию';
		$company->users()->remove($egor);
		if ($company->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Обновление юзеров через компанию';
		$company->users()->update([
			'phone' => '89002000600',
		]);
		if ($company->users->first()->phone == '89002000600') {
			$lines[]= '<span class="text-success">'.print_r($company->users->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($company->users->first()->toArray(), 1).'</span>';
		}
		$lines[]= 'Удаление юзеров компании';
		$company->users()->remove();
		if ($company->users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Удаление компании';
		$company_id = $company->id;
		$company->remove();
		$company = Company::find($company_id);
		if (!is_object($company)) {
			$lines[]= '<span class="text-success">Удалена</span>';
		} else {
			$lines[]= '<span class="text-danger">Не удалена</span>';
		}
		$lines[]= 'Конец';
		view('main.tests/test', [
			'title' => $this->request->input('run'),
			'lines' => $lines,
		]);
	}
	
    private function UserCompany()
    {
		$lines = [];
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Удаление всех компаний';
		Company::removeAll();
		$companys = Company::get();
		if ($companys->count() == 0) {
			$lines[]= '<span class="text-success">Компаний: '.$companys->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Компаний: '.$companys->count().'</span>';
		}
		$lines[]= 'Создание юзера: Жорик, test@f5.com.ru';
		$jora = User::create([
			'name' => 'Жорик',
			'login' => 'test@f5.com.ru',
		]);
		if (is_object($jora)) {
			$lines[]= '<span class="text-success">'.print_r($jora->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($jora->toArray(), 1).'</span>';
		}
		$lines[]= 'Компания Жорика';
		if (!is_object($jora->company)) {
			$lines[]= '<span class="text-success">Нет</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($jora->company->toArray(), 1).'</span>';
		}
		$lines[]= 'Создание компании: Команда F5';
		$company = Company::create([
			'name' => 'Команда F5',
		]);
		if (is_object($company)) {
			$lines[]= '<span class="text-success">'.print_r($company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($company->toArray(), 1).'</span>';
		}
		if ($company->users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Прикрепление Жорика к компании: Команда F5';
		$jora->company()->attach($company);
		if ($company->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Компания Жорика';
		if (is_object($jora->company)) {
			$lines[]= '<span class="text-success">'.print_r($jora->company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Нет</span>';
		}
		$lines[]= 'Создание юзера через компанию: Егор, egor@f5.com.ru';
		$egor = $company->users()->create([
			'name' => 'Егор',
			'login' => 'egor@f5.com.ru',
		]);
		if (is_object($egor)) {
			$lines[]= '<span class="text-success">'.print_r($egor->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.print_r($egor->toArray(), 1).'</span>';
		}
		$lines[]= 'Проверка кол-ва юзеров компании';
		if ($company->users->count() == 2) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Открепление Егора от компании: Команда F5';
		$egor->company()->detach();
		if ($company->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$company->users->count().'</span>';
			$lines[]= '<span class="text-success">'.print_r($company->users->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$company->users->count().'</span>';
		}
		$lines[]= 'Компания Егора';
		if (!is_object($egor->company) && is_null($egor->company_id)) {
			$lines[]= '<span class="text-success">Нет</span>';
		} else {
			$lines[]= '<span class="text-danger">Да '.print_r($egor->company->toArray(), 1).'</span>';
		}
		$lines[]= 'Компания Жорика';
		if (is_object($jora->company)) {
			$lines[]= '<span class="text-success">Да '.print_r($jora->company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Нет</span>';
		}
		$lines[]= 'Создание компании через Егора: Агро';
		$agro = $egor->company()->create([
			'name' => 'Агро',
		]);
		$lines[]= 'Компания Егора';
		if (is_object($egor->company) && $egor->company->name == 'Агро') {
			$lines[]= '<span class="text-success">'.print_r($egor->company->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">'.$egor->company_id.': '.print_r($egor->company->toArray(), 1).'</span>';
			exit;
		}
		$lines[]= 'Проверка кол-ва юзеров компании Агро';
		if ($egor->company->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров компании: '.$egor->company->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров компании: '.$egor->company->users->count().'</span>';
		}
		$lines[]= 'Удаление компании Команда F5 через Жорика';
		$company_id = $company->id;
		$jora->company()->remove();
		$lines[]= 'Компания Жорика';
		if (!is_object($jora->company)) {
			$lines[]= '<span class="text-success">Нет</span>';
		} else {
			$lines[]= '<span class="text-danger">Да '.print_r($jora->company->toArray(), 1).'</span>';
		}
		$company = Company::find($company_id);
		if (!is_object($company)) {
			$lines[]= '<span class="text-success">Компания удалена</span>';
		} else {
			$lines[]= '<span class="text-danger">Компания НЕ удалена</span>';
		}
		$lines[]= 'Удаление компании Агро';
		$egor->company->remove();
		$companys = Company::get();
		if ($companys->count() == 0) {
			$lines[]= '<span class="text-success">Компаний: '.$companys->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Компаний: '.$companys->count().'</span>';
		}
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Конец';
		view('main.tests/test', [
			'title' => $this->request->input('run'),
			'lines' => $lines,
		]);
	}
	
    private function UserRoles()
    {
		$lines = [];
		$lines[]= 'Удаление всех ролей';
		Role::removeAll();
		$roles = Role::get();
		if ($roles->count() == 0) {
			$lines[]= '<span class="text-success">Ролей: '.$roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$roles->count().'</span>';
		}
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$lines[]= 'Создание ролей: Администратор, Модератор';
		$roles = Role::insert([
			['name' => 'Администратор', 'code' => 'admin'],
			['name' => 'Модератор', 'code' => 'moder']
		]);
		if ($roles->count() == 2) {
			$lines[]= '<span class="text-success">Ролей: '.$roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$roles->count().'</span>';
		}
		$lines[]= 'Создание юзеров: Егор, Василий';
		$users = User::insert([
			['name' => 'Егор', 'login' => 'egor'],
			['name' => 'Василий', 'login' => 'vass']
		]);
		if ($users->count() == 2) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		$admin = Role::find($roles->find('code', 'admin')->first()->id);
		$lines[]= 'Связанные юзеры роли: Администратор';
		if ($admin->users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$admin->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$admin->users->count().'</span>';
		}
		$moder = Role::find($roles->find('code', 'moder')->first()->id);
		$lines[]= 'Связанные юзеры роли: Модератор';
		if ($moder->users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$moder->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$moder->users->count().'</span>';
		}
		$egor = User::find($users->find('login', 'egor')->first()->id);
		$lines[]= 'Связанные роли юзера: Егор';
		if ($egor->roles->count() == 0) {
			$lines[]= '<span class="text-success">Ролей: '.$egor->roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$egor->roles->count().'</span>';
		}
		$vass = User::find($users->find('login', 'vass')->first()->id);
		$lines[]= 'Связанные роли юзера: Василий';
		if ($vass->roles->count() == 0) {
			$lines[]= '<span class="text-success">Ролей: '.$vass->roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$vass->roles->count().'</span>';
		}
		$lines[]= 'Присвоим Егору роль Администратор';
		$egor->roles()->attach($admin);
		if ($egor->hasRole('admin')) {
			$lines[]= '<span class="text-success">Егор Администратор</span>';
			$lines[]= '<span class="text-success">'.print_r($egor->roles->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Егор НЕ Администратор</span>';
		}
		$lines[]= 'Присвоим Василию роль Модератор';
		$vass->roles()->attach($moder);
		if ($vass->hasRole('moder')) {
			$lines[]= '<span class="text-success">Василий Модератор</span>';
			$lines[]= '<span class="text-success">'.print_r($vass->roles->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Василий НЕ Модератор</span>';
		}
		$lines[]= 'Связанные юзеры роли: Администратор';
		if ($admin->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров: '.$admin->users->count().'</span>';
			$lines[]= '<span class="text-success">'.print_r($admin->users->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$admin->users->count().'</span>';
		}
		$moder = Role::find($roles->find('code', 'moder')->first()->id);
		$lines[]= 'Связанные юзеры роли: Модератор';
		if ($moder->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров: '.$moder->users->count().'</span>';
			$lines[]= '<span class="text-success">'.print_r($moder->users->first()->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$moder->users->count().'</span>';
		}
		$lines[]= 'Присвоим Администраторам юзера Василий';
		$admin->users()->attach($vass);
		if ($vass->hasRole('admin')) {
			$lines[]= '<span class="text-success">Василий Администратор</span>';
		} else {
			$lines[]= '<span class="text-danger">Василий НЕ Администратор</span>';
		}
		$lines[]= 'Связанные роли юзера: Василий';
		if ($vass->roles->count() == 2) {
			$lines[]= '<span class="text-success">Ролей: '.$vass->roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$vass->roles->count().'</span>';
		}
		$lines[]= 'Связанные юзеры роли: Администратор';
		if ($admin->users->count() == 2) {
			$lines[]= '<span class="text-success">Юзеров: '.$admin->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$admin->users->count().'</span>';
		}
		$lines[]= 'Уберем у Василия роль Администратора';
		$vass->roles()->detach($admin);
		if ($vass->hasRole('admin')) {
			$lines[]= '<span class="text-danger">Василий Администратор</span>';
		} else {
			$lines[]= '<span class="text-success">Василий НЕ Администратор</span>';
		}
		$lines[]= 'Связанные юзеры роли: Администратор';
		if ($admin->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров: '.$admin->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$admin->users->count().'</span>';
		}
		$lines[]= 'Уберем из Администраторов юзера Егор';
		$admin->users()->detach($egor);
		$lines[]= 'Связанные юзеры роли: Администратор';
		if ($admin->users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$admin->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$admin->users->count().'</span>';
		}
		if ($egor->hasRole('admin')) {
			$lines[]= '<span class="text-danger">Егор Администратор</span>';
		} else {
			$lines[]= '<span class="text-success">Егор НЕ Администратор</span>';
		}
		$lines[]= 'Создадим юзера в роли Администратор: Жора';
		$jora = $admin->users()->create([
			'name' => 'Жора', 'login' => 'jora'
		]);
		if (is_object($jora) && $jora->login == 'jora') {
			$lines[]= '<span class="text-success">Создан</span>';
			$lines[]= '<span class="text-success">'.print_r($jora->toArray(), 1).'</span>';
		} else {
			$lines[]= '<span class="text-danger">НЕ создан</span>';
		}
		if ($jora->hasRole('admin')) {
			$lines[]= '<span class="text-success">Жора Администратор</span>';
		} else {
			$lines[]= '<span class="text-danger">Жора НЕ Администратор</span>';
		}
		$lines[]= 'Связанные юзеры роли: Администратор';
		if ($admin->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров: '.$admin->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$admin->users->count().'</span>';
		}
		$lines[]= 'Связанные роли юзера: Егор';
		if ($egor->roles->count() == 0) {
			$lines[]= '<span class="text-success">Ролей: '.$egor->roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$egor->roles->count().'</span>';
		}
		$lines[]= 'Создадим роль Гость через Егора';
		$guest = $egor->roles()->create([
			'name' => 'Гость', 'code' => 'guest'
		]);
		if ($egor->hasRole('guest')) {
			$lines[]= '<span class="text-success">Егор Гость</span>';
		} else {
			$lines[]= '<span class="text-danger">Егора Гость</span>';
		}
		$lines[]= 'Связанные юзеры роли: Гость';
		if ($guest->users->count() == 1) {
			$lines[]= '<span class="text-success">Юзеров: '.$guest->users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$guest->users->count().'</span>';
		}
		$lines[]= 'Удаление всех ролей';
		Role::removeAll();
		$roles = Role::get();
		if ($roles->count() == 0) {
			$lines[]= '<span class="text-success">Ролей: '.$roles->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Ролей: '.$roles->count().'</span>';
		}
		$lines[]= 'Удаление всех юзеров';
		User::removeAll();
		$users = User::get();
		if ($users->count() == 0) {
			$lines[]= '<span class="text-success">Юзеров: '.$users->count().'</span>';
		} else {
			$lines[]= '<span class="text-danger">Юзеров: '.$users->count().'</span>';
		}
		view('main.tests/test', [
			'title' => $this->request->input('run'),
			'lines' => $lines,
		]);
	}
	
	/**
	 * Test 
	 */
    public function test()
    {
		$user = User::find(1);
		$user->save();
		print_r($user);
		var_dump($user->hasPassword('1234'));
		
		exit;
		echo "\n\n-- Resource: --\n\n";
		
		print_r($user->roles);
		
		echo "\n\n-- Wrapper: --\n\n";
		
		print_r($user->roles());
		
		echo "\n\n-- Action: --\n\n";
		
		$role = Role::find(4);
		print_r($role->users);
		
		$role->users()->attach($user);

		echo "\n\n-- Result: --\n\n";
		
		print_r($role->users);
		print_r($user->roles);
		
		
	}
	
	/**
	 * Test belongsToMany
	 */
    public function belongsToManyTest()
    {
		$role = Role::find(3);
		echo "\n\n-- Role: --\n\n";
		print_r($role);
		
		echo "\n\n-- Role Users --\n\n";
		print_r($role->users->all());
		
		echo "\n\n-- Role Users2 --\n\n";
		print_r($role->users->all());
		
		$role->users->detach();
		
		echo "\n\n-- Role Users detached --\n\n";
		print_r($role->users->all());
		
		$role->users->attach(2);
		
		echo "\n\n-- Role Users attached 2 --\n\n";
		print_r($role->users->all());
		
		echo "\n\n-- Role Users --\n\n";
		print_r($role->users->all());
		
		$role->users->attach([1,3]);
		
		echo "\n\n-- Role Users attached 1,3 --\n\n";
		print_r($role->users->all());
		
		
		echo "\n\n--  --\n\n";
	}
	
	/**
	 * Test hasMany
	 */
    public function hasManyTest()
    {
		$user = User::find(3);
		echo "\n\n-- User: --\n\n";
		print_r($user);
		
		echo "\n\n-- User Roles --\n\n";
		print_r($user->roles->all());
		
		echo "\n\n-- User Roles2 --\n\n";
		print_r($user->roles->all());
		
		$user->roles->detach();
		
		echo "\n\n-- User Roles detached --\n\n";
		print_r($user->roles->all());
		
		$user->roles->attach(2);
		
		echo "\n\n-- User Roles attached 2 --\n\n";
		print_r($user->roles->all());
		
		echo "\n\n-- User Roles --\n\n";
		print_r($user->roles->all());
		
		$user->roles->attach([1,3]);
		
		echo "\n\n-- User Roles attached 1,3 --\n\n";
		print_r($user->roles->all());
		
		echo "\n\n--  --\n\n";
	}
	
    private function OpenSSL()
    {
		$vars = [
			' ',
			123,
			123.456,
			156754567869325,
			'test |',
			'test | ',
			' test |',
			' test /',
			' test \\',
			' test //',
			' test %',
			' test % ',
			' test % =',
			' test %/==,',
			' test %/==.',
			'/ /',
			'=%',
			'"',
			' test " ::',
			' test " ] * # $ ! @ ^ & ?',
			' Ge%20a cD0m+o fH/ig+P Y0==',
			'-=+'
		];
		$lines = [];
		foreach ($vars as $var) {
			$lines[]= 'Значение: "'.$var."\"\n";
			$encoded = encrypt($var);
			$decoded = decrypt($encoded);
			if ($decoded === $var) {
				$lines[]= '<span class="text-success">'.$encoded.' -> "'.$decoded.'"</span>';
			} else {
				$lines[]= '<span class="text-success">'.$encoded.' -> "'.$decoded.'"</span>';
			}
		}
		$lines[]= 'Конец';
		view('main.tests/test', [
			'title' => $this->request->input('run'),
			'lines' => $lines,
		]);
	}
}
