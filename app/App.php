<?php
/**
 * Web Application stages
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App;
use Core\Application;

class App extends \Core\Containers\AppContainer
{
	protected function _boot(Application $app)
	{
		// $app->registerModel('App\Models\User', 'user');
		$app->registerInstance('Classes\Client', 'client');
	}
	protected function _shutdown(Application $app)
	{
		$execution_time = round((microtime(true) - $app->startTime(true)), 5);
		$memory_usage = memory_get_peak_usage(true)/1024/1024;
		
		//echo '<div><b>Execution time:</b> '.$execution_time.' sec</div>',
		//	 '<div><b>Memory usage:</b> '.$memory_usage.' mb</div>';
	}
}
