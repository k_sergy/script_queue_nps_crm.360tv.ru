<?php
/**
 * Web Application Company Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Models;

class Company extends \Core\Models\DbModel
{
	use \Core\Models\Traits\Has;
	
	protected 
		$hidden = [],
		$writable = ['name'];
	
    /**
     * Get Company Users
	 * @return Collection
     */
	public function users()
	{
		return $this->hasLinked(User::class);
	}
}
