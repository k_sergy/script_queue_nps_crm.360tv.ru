<?php
/**
 * Web Application Event Listener
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Events\Listeners;
use App\Events\Event;

class EventListener
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // code ...
    }

    /**
     * Handle event
     * @param Event $event
     */
    public function handle(Event $event)
    {
        // code ...
    }
}
