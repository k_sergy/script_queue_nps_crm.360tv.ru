<?php
/**
 * CRM Widget events subscriber
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Events\Subscribers;
use Core\Models\Event,
	Core\Application,
	Components\Request,
	Components\Logger,
	App\Jobs\SetFieldLead,
	Components\Queue;

abstract class Hook
{
    /**
     * Widget installation handler
     * @param Event $event
     */
	public static function onEvent(Event $event)
	{
		$l = logger('/hook/events.log');
		$l->log($event);
		$job = new  SetFieldLead($event->hook);
		// $job->handle();
		$qjob = queue()->push($job, 'setfieldlead');
		$l->log($qjob->id);
		$qjob->setState(0);
	}
	
   /**
     * Events subscriber
	 * @param Events $events
     */
	public static function subscribe(\Components\Events $events)
	{
		$events->on('hook.event', static::class.'@onEvent');
	}
}
