<?php
/**
 * Web Application Route boot
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App;

class Routes extends \Core\Containers\RoutesContainer
{
	protected function web(\Components\Route $route)
	{
		$route->post('amowebhook', 'App\Controllers\AmoWebhook@index');
		$route->get('test', 'App\Controllers\Tests@index');
	}
	
	protected function cli(\Components\Route $route)
	{
		
	}
}
