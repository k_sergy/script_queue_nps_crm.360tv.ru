<?php
/**
 * Some job
 */
namespace App\Jobs;
use Classes\Client;

class SetFieldLead extends \Core\Jobs\QueueJob
{

	protected $hook;
	protected $client;

	public function __construct($hook)
	{
		$this->lead = $hook;
	}
	/**
	 * Handle job
	 */
    public function handle()
    {
    	$l = logger('/hook/setfield.log');
    	// $l->log($this->lead);
	
		$this->client = app('client');

		$amo = $this->client->crm();

		$leadID = $this->lead['id'];
		$l->log('LeadID = ' . $leadID);
		$lead = $amo->leads()->find($leadID);
		if(!$lead){
			$l->log("Сделка не найдена");
			return true;
		}
		$company = $lead->company;

		$l->log('CompanyID = ' . $company->id);

		$date = $company->cf('Дата последнего касания NPS')->getValue();

		$now = time(); 
		$date = strtotime($date);
		$datediff = abs($now - $date);

		$daydiff = ceil($datediff / (60 * 60 * 24));

		$l->log('DayDiff = ' . $daydiff);
		$text_cf = '';
		if ($daydiff <= 90) {
			$text_cf = 'Нет';
		} else {
			$text_cf = 'Да';
		}
		$l->log('Отправлять NPS = ' . $text_cf);

		$company->leads->each(function(&$lead) use ($text_cf) { 
			if(!$lead->status->hasClosed()){
				$lead->cf('Отправлять NPS')->setValue($text_cf);
				$lead->save();
			}
		});
	}
}
