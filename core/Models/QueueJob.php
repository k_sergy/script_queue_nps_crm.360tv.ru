<?php
/**
 * Web Application Queue Job
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models;
use Components\Database\Connection as Db;
use Ufee\Sqlite3\Sqlite;

class QueueJob extends DbModel
{
	const GZIP_LEVEL = 7;
	
	protected static 
		$_table_name = 'queue_job';
	protected 
		$writable = [
			'private',
			'name',
			'attempt',
			'state',
			'completed_at',
			'aviable_at',
			'priority'
		],
		$_table,
		$_job;
	
    /**
     * Set private status
	 * @param integer $val
	 * @return QueueJob
     */
	public function setPrivate($val = 1)
	{
		$this->update(['private' => $val]);
		return $this;
	}
	
    /**
     * Set priority
	 * @param integer $val
	 * @return QueueJob
     */
	public function priority($val)
	{
		$this->update(['priority' => $val]);
		return $this;
	}
	
    /**
     * Set aviable datetime
	 * @param string $val
	 * @return QueueJob
     */
	public function aviableAt($val)
	{
		$this->update(['aviable_at' => $val]);
		return $this;
	}
	
	/**
	 * Set job state
	 * @param integer $val
	 * @return QueueJob
	 */
    public function setState($val)
    {
		$this->update(['state' => $val]);
		return $this;
	}
	
    /**
     * Set delay datetime
	 * @param integer $val in seconds
	 * @return QueueJob
     */
	public function delay($val)
	{
		$delay_time = getTime();
		if ($this->aviable_at && $this->aviable_at > dateTime()->format('Y-m-d H:i:s')) {
			$delay_time = strtotime($this->aviable_at);
		}
		return $this->aviableAt(
			dateTime()->setTimestamp($delay_time+$val)->format('Y-m-d H:i:s')
		);
	}
	
    /**
     * Is job completed
	 * @return bool
     */
	public function hasCompleted()
	{
		return !is_null($this->completed_at) && intval($this->state) !== 0;
	}
	
    /**
     * Is job aviable
	 * @return bool
     */
	public function hasAviableNow()
	{
		return is_null($this->aviable_at) || strtotime($this->aviable_at) <= getTime();
	}
	
    /**
     * Is job failed
	 * @return bool
     */
	public function hasFailed()
	{
		return $this->getJob()->hasFailed();
	}
	
	/**
	 * Get job exception
	 * @return Exception
	 */
    public function getException()
    {
		return $this->getJob()->getException();
    }
	
	/**
	 * Remove job exception
	 * @return Job
	 */
    public function removeException()
    {
		return $this->getJob()->removeException();
    }
	
    /**
     * Get Job object
	 * @return Job
     */
	public function getJob()
	{
		if (is_null($this->_job)) {
			if (!$data = $this->table()->select('data')->where('job_id', $this->id)->row('data')) {
				throw new \Exception('Queue sqlite data Job not found');
			}
			if (!$decoded = gzdecode($data)) {
				throw new \Exception('Queue sqlite data Job gzdecode error');
			}
			if (!$this->_job = unserialize($decoded)) {
				throw new \Exception('Queue sqlite data Job unserialize error');
			}
		}
		return $this->_job;
	}
	
    /**
     * Set job object
	 * @param Job $job
	 * @return bool
     */
	public function setJob(\Core\Jobs\QueueJob $job)
	{
		$this->_job = $job;
		return $this->table()->insert('job_id, data')
							 ->orRreplace()
							 ->row([
								$this->id, 
								gzencode(serialize($job), static::GZIP_LEVEL)
							 ]);
	}
	
	/**
	 * Process this job
	 * @param bool $force
	 */
    public function process($force = false)
    {
		return queue()->process($this, $force);
	}
	
    /**
     * Get Sqlite storage
	 * @return Table
     */
	public function table()
	{
		if (is_null($this->_table)) {
			$db = Sqlite::database(STORAGE.'/Queue/'.$this->name.'.db', [
				'busy_timeout' => 60
			]);
			if (!$db->fileExists()) {
				$db->create();
				$db->table('queue')->create([
					'job_id' => 'INTEGER UNIQUE', 
					'data' => 'BLOB'
				]);
			}
			$this->_table = $db->table('queue');
			$this->_table->setColumnType('job_id', 'INT');
			$this->_table->setColumnType('data', 'BLOB');
		}
		return $this->_table;
	}
}
