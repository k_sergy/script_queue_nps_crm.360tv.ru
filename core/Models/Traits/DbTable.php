<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;

trait DbTable
{
    /**
     * Get Model increment column name
     * @return string
     */
    public static function incrColName()
    {
		return static::$_incr;
    }
	
    /**
     * Get Model linked column name
     * @return string
     */
    public static function linkedColName()
    {
		return is_null(static::$_linked_column) ? static::getBasename().'_'.static::$_incr : static::$_linked_column;
    }
	
    /**
     * Get Model table
     * @return string
     */
    public static function getTableName()
    {
		return is_null(static::$_table_name) ? 'model_'.static::getBasename() : static::$_table_name;
    }
	
    /**
     * Get Model linked table
	 * @param string $target
     * @return string
     */
    public static function hasManyTableName($target)
    {
		return (is_null(static::$_has_many_prefix) ? static::getBasename() : static::$_has_many_prefix).'_to_'.(is_null($target::$_belongs_to_many_suffix) ? $target::getBasename() : $target::$_belongs_to_many_suffix);
    }
	
    /**
     * Get Model linked table
	 * @param string $parent
     * @return string
     */
    public static function belongsToManyTableName($parent)
    {
		return (is_null($parent::$_belongs_to_many_suffix) ? $parent::getBasename() : $parent::$_belongs_to_many_suffix).'_to_'.(is_null(static::$_has_many_prefix) ? static::getBasename() : static::$_has_many_prefix);
    }
}
