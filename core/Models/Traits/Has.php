<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;
use Components\Database\Buffer,
	Core\Models\Collections,
	Core\Models\Wrappers,
	Core\Models\LinkedModel,
	Components\Database\Connection as Db;

trait Has
{
    /**
     * Получить младшие модели - многие ко многим
	 * @param string $target класс младшей модели
	 * @param arrray $linked_fields - получаем поля из таблицы связи
	 * @return Wrapper
     */
	public function hasMany($target, $linked_fields = [])
	{
		return $this->hasManyHandler($target, $linked_fields);
	}
	
    /**
     * Получить связанные младшие модели
	 * @param string $target класс младшей модели
	 * @param mixed $linked_col столбец связи
	 * @return HasLinkedCollection
     */
	public function hasLinked($target, $linked_col = null)
	{
		return $this->hasLinkedHandler($target, $linked_col);
	}
	
    /**
     * Получить связанную младшую модель
	 * @param string $target класс младшей модели
	 * @param mixed $linked_col столбец связи
	 * @return Model
     */
	public function hasOne($target, $linked_col = null)
	{
		if (!is_string($linked_col)) {
			$linked_col = static::linkedColName();
		}
		$buffer_key = static::hasLinkedKey($target, $this->{$this::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->where($linked_col, $this->{$this::incrColName()})
							  ->fromTable($target::getTableName())
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$object = new $target($data);
				$models[]= $object;
			}
        }
		$collection = new Collections\HasOne($models, $target, $this, $linked_col);
		$wrapper = $collection->wrapper();

		Buffer::set($buffer_key, $wrapper);
		return $wrapper;
	}
	
    /**
     * Связи с младшими моделями
	 * @param string $target класс младшей модели
	 * @param mixed $linked_col столбец связи
	 * @return HasLinkedCollection
     */
	public function hasLinkedHandler($target, $linked_col = null)
	{
		if (!is_string($linked_col)) {
			$linked_col = static::linkedColName();
		}
		return new \Core\Models\Handlers\HasLinked($target, $this, $linked_col);
	}
	
    /**
     * Связи с младшими моделями - много ко много
	 * @param string $target класс младшей модели
	 * @param mixed $linked_col столбец связи
	 * @return HasLinkedCollection
     */
	public function hasManyHandler($target, $linked_fields = [])
	{
		return new \Core\Models\Handlers\HasMany($target, $this, $linked_fields);
	}
}
