<?php
/**
 * Web Application Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models;

class Model
{
	use Traits\Detector, Traits\Converter;
	
	protected 
		$system = [],
		$hidden = [],
		$writable = [],
		$attributes = [];
		
    /**
     * Constructor
	 * @param mixed $data
     */
    public function __construct($data = [])
    {
		if (!is_array($data) && !is_object($data)) {
			throw new \Exception(static::getBasename().' model data must be array or object');
		}
        foreach ($this->system as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($this->hidden as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($this->writable as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($data as $field_key=>$val) {
			if (array_key_exists($field_key, $this->attributes)) {
				$this->attributes[$field_key] = $val;
			}
		}
		$this->_boot();
    }
	
    /**
     * Model on load
     */
    protected function _boot()
    {
		// code...
	}
	
    /**
     * Has model attribute
	 * @param string $field_key
	 * @return bool
     */
	public function hasAttribute($field_key)
	{
		return array_key_exists($field_key, $this->attributes);
	}
	
    /**
     * Get model writable fields
	 * @return array
     */
	public function writableFields()
	{
		return array_merge($this->writable, $this->hidden);
	}
	
    /**
     * Get write fields data
     * @return array
     */
    protected function _getWriteAttributes()
    {
		$fields = [];
		$writable = $this->writableFields();
        foreach ($this->attributes as $key=>$val) {
			if (!in_array($key, $writable)) {
				continue;
			}
			$fields[$key] = $val;
		}
		return $fields;
    }
	
	/**
     * Protect get model fields
	 * @param string $field
     */
	public function __get($field)
	{
		if (!array_key_exists($field, $this->attributes)) {
			
			if (method_exists($this, $field)) {
				
				$result = $this->$field();
				if ($result instanceof \Core\Models\Wrappers\Model) {
					return $result->resource();
				}
				if ($result instanceof \Core\Models\Handlers\Handler) {
					return $result->getResource();
				}
				return $result;
			}
			throw new \Exception('Invalid '.static::getBasename().' field: '.$field);
		}
		if ($field == 'linked') {
			if (!($this->attributes['linked'] instanceOf \Core\Models\LinkedModel)) {
				throw new Exception('Linked fields must be called in LinkedModel context only');
			}
		}
		return $this->attributes[$field];
	}

    /**
     * Protect set model fields
	 * @param string $property
	 * @param string $value
     */
	public function __set($field, $value)
	{
		if (!array_key_exists($field, $this->attributes)) {
			throw new \Exception('Invalid '.static::getBasename().' field: '.$field);
		}
		if (!in_array($field, $this->writable)) {
			throw new \Exception('Protected '.static::getBasename().' field set fail: '.$field);
		}
		$this->attributes[$field] = $value;
	}
}
