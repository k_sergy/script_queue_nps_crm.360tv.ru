<?php
/**
 * Web Application Handler - Обработчик связи с младшими моделями
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Handlers;
use Core\Models\Collections,
	Components\Database\Connection as Db,
	Components\Database\Buffer;

class HasLinked extends Handler
{
    /**
     * Создание младших сущностей с привязкой к старшей
	 * @param array|object $data
	 * @return Model
     */
	public function insert($data = [])
	{
		if (is_object($data)) {
			$data = (array)$data;
		}
		$current = get_class($this->currmodel);
		$target = $this->target;
		
		foreach ($data as &$item) {
			$item[$this->linked_col] = $this->currmodel->{$current::incrColName()};
		}
		$models = $target::insert($data);
		Buffer::remove($current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()}));		
		return $models;
	}
	
    /**
     * Создание младшей сущности с привязкой к старшей
	 * @param array|object $data
	 * @return Model
     */
	public function create($data = [])
	{
		if (is_object($data)) {
			$data = (array)$data;
		}
		$current = get_class($this->currmodel);
		$target = $this->target;
		
		$data[$this->linked_col] = $this->currmodel->{$current::incrColName()};
		$model = $target::create($data);

		Buffer::remove($current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()}));
		return $model;
	}
	
    /**
     * Обновление младших сущностей
	 * @param array|object $update
	 * @return Wrapper
     */
	public function update($update = [])
	{
		if (is_object($update)) {
			$update = (array)$update;
		}
		$current = get_class($this->currmodel);
		$target = $this->target;
		
		$emptyModel = new $target([]);
		$updateFields = collection(array_keys($update));
		$writableFields = $emptyModel->writableFields();
		$update['modified_at'] = date('Y-m-d H:i:s', getTime());
		
		$failed = $updateFields->filter(function($value, $key) use($writableFields) {
			return !in_array($value, $writableFields);
		});
		if ($failed->count() > 0) {
			throw new \Exception('Non-writable model fields detected: '.$failed->join(', '));
		}
		Db::link()->where($this->linked_col, $this->currmodel->{$current::incrColName()})
				  ->update($target::getTableName(), $update);
		
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()}));
			return $this;
		}
		throw new \Exception('Error update models: '.Db::link()->getLastError());
	}
	
    /**
     * Удаление младших сущностей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function remove($increments = null)
	{
		$current = get_class($this->currmodel);
		$target = $this->target;

		Db::link()->where($this->linked_col, $this->currmodel->{$current::incrColName()});
		if (!is_null($increments)) {
			$target_increments = $this->getIncrementsArray($increments);
			Db::link()->where($target::incrColName(), $target_increments, 'IN');
		}
		Db::link()->delete($target::getTableName());

		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()}));
			return $this;
		}
		throw new \Exception('Error remove models: '.Db::link()->getLastError());
	}
	
    /**
     * Привязка младших сущностей к старшей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function attach($increments)
	{
		$current = get_class($this->currmodel);
		$target = $this->target;
		$target_models = $this->getTargetModelsArray($increments);
		
		$update = [];
		$update[$this->linked_col] = $this->currmodel->{$current::incrColName()};
		$target_increments = $this->getIncrementsArray($increments);

		Db::link()->where($target::incrColName(), $target_increments, 'IN')
				  ->update($target::getTableName(), $update);
				  
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()}));
			foreach ($target_models as &$model) {
				$model->{$this->linked_col} = $this->currmodel->{$current::incrColName()};
			}
			return $this;
		}
		throw new \Exception('Error attach models: '.Db::link()->getLastError());
	}
	
    /**
     * Отвязка младших сущностей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function detach($increments = null)
	{
		$current = get_class($this->currmodel);
		$target = $this->target;
		$target_models = [];
		
		$update = [];
		$update[$this->linked_col] = null;
		
		Db::link()->where($this->linked_col, $this->currmodel->{$current::incrColName()});
		if (!is_null($increments)) {
			$target_models = $this->getTargetModelsArray($increments);
			$target_increments = $this->getIncrementsArray($increments);
			Db::link()->where($target::incrColName(), $target_increments, 'IN');
		}
		Db::link()->update($target::getTableName(), $update);

		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()}));
			foreach ($target_models as &$model) {
				$model->{$this->linked_col} = null;
			}
			return $this;
		}
		throw new \Exception('Error detach models: '.Db::link()->getLastError());
	}
	
    /**
     * Clear cache
	 * @return bool
     */
    public function clearCache()
    {
		$current = get_class($this->currmodel);
		$target = $this->target;
		$buffer_key = $current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()});
		return Buffer::remove($buffer_key);
	}
	
    /**
     * Get models
	 * @return Collection
     */
    public function getResource()
    {
		$current = get_class($this->currmodel);
		$target = $this->target;
		$buffer_key = $current::hasLinkedKey($target, $this->currmodel->{$current::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->where($this->linked_col, $this->currmodel->{$current::incrColName()})
							  ->fromTable($target::getTableName())
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$object = new $target($data);
				$models[]= $object;
			}
        }
		$collection = new Collections\HasLinked($models);
		Buffer::set($buffer_key, $collection);
		return $collection;
	}
}
