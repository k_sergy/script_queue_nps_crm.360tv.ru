<?php
/**
 * Web Application Handler - Обработчик связи с младшими моделями - много ко много
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Handlers;
use Core\Models\Collections,
	Components\Database\Connection as Db,
	Core\Models\LinkedModel,
	Components\Database\Buffer;

class HasMany extends Handler
{
	protected $linked_fields;
	
    /**
     * Constructor
	 * @param string $target class
	 * @param object $currmodel object
	 * @param mixed $linked_fields доп.поля
     */
    public function __construct($target, $currmodel, $linked_fields = [])
    {
		$this->target = $target;
		$this->currmodel = $currmodel;
		$this->linked_fields = $linked_fields;
	}
	
    /**
     * Создание младших сущностей с привязкой к старшей
	 * @param array|object $data
	 * @return Model
     */
	public function create($data = [])
	{
		if (is_object($data)) {
			$data = (array)$data;
		}
		$current = get_class($this->currmodel);
		$target = $this->target;
		
		$object = $target::create($data);
		$this->attach($object);
		return $object;
	}
	
    /**
     * Создание связей между сущностями
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function attach($increments, $linked_data = [])
	{
		$current = get_class($this->currmodel);
		$target = $this->target;

		$attaches = [];
		$target_models = $this->getTargetModelsArray($increments);
		$increment = $this->getIncrementsArray($increments);
		
		if (empty($target_models)) {
			throw new \Exception('Error attach models (target models: is empty or unsupported)');
		}
		if (empty($increment)) {
			throw new \Exception('Error attach models (target increment: is empty or unsupported)');
		}
		$linkeds = [];
		if (!empty($linked_data)) {
			foreach ($linked_data as $field=>$val) {
				if (in_array($field, $this->linked_fields)) {
					$linkeds[$field] = $val;
				}
			}
		}
		$attach_fields = [
			'link_id', $current::linkedColName(), $target::linkedColName()
		];
		foreach ($linkeds as $field=>$val) {
			$attach_fields[]= $field;
		}
		foreach ($increment as $attach_id) {
			$attach_values = [
				'"'.md5($this->currmodel->{$current::incrColName()}.'_'.$attach_id).'"',
				Db::link()->escape($this->currmodel->{$current::incrColName()}),
				Db::link()->escape($attach_id)
			];
			foreach ($linkeds as $field=>$val) {
				$attach_values[]= "'".Db::link()->escape($val)."'";
			}
			$attaches[]= '('.implode(',', $attach_values).')';
		}
		$sql = 'INSERT IGNORE INTO '.$current::hasManyTableName($target).' ('.join(', ', $attach_fields).') VALUES '.join(', ', $attaches);
		Db::link()->rawQuery($sql);
		
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasManyKey($target, $this->currmodel->{$current::incrColName()}));
			foreach ($target_models as $model) {
				Buffer::remove($target::BelongsToManyKey($current, $model->{$model::incrColName()}));
			}
			return $this;
		}
		throw new \Exception('Error attach models: '.Db::link()->getLastError());
	}
	
    /**
     * Разрыв связей между сущностями
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function detach($increments = null)
	{
		$current = get_class($this->currmodel);
		$target = $this->target;
		$target_models = $this->getTargetModelsArray($increments);

		Db::link()->where($current::linkedColName(), $this->currmodel->{$current::incrColName()});
		if (!is_null($increments)) {
			$whereIds = $this->getIncrementsArray($increments);
			if (empty($whereIds)) {
				throw new \Exception('Error detach: empty target ids');
			}
			Db::link()->where($target::linkedColName(), $whereIds, 'IN');
		}
		Db::link()->delete($current::hasManyTableName($target));
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasManyKey($target, $this->currmodel->{$current::incrColName()}));
			foreach ($target_models as $model) {
				Buffer::remove($target::BelongsToManyKey($current, $model->{$model::incrColName()}));
			}
			return $this;
		}
		throw new \Exception('Error detach models: '.Db::link()->getLastError());
	}
	
    /**
     * Clear cache
	 * @return bool
     */
    public function clearCache()
    {
		$current = get_class($this->currmodel);
		$target = $this->target;
		$buffer_key = $current::hasManyKey($target, $this->currmodel->{$current::incrColName()});
		return Buffer::remove($buffer_key);
	}
	
    /**
     * Get models
	 * @return Collection
     */
    public function getResource()
    {
		$current = get_class($this->currmodel);
		$target = $this->target;
		$buffer_key = $current::hasManyKey($target, $this->currmodel->{$current::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->join($current::hasManyTableName($target).' m', 't.'.$target::incrColName().'=m.'.$target::linkedColName(), 'LEFT')
							  ->where('m.'.$current::linkedColName(), $this->currmodel->{$current::incrColName()})
							  ->fromTable($target::getTableName().' t')
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$linked_values = ['link_id' => $data['link_id']];
				foreach ($this->linked_fields as $key) {
					$linked_values[$key] = isset($data[$key]) ? $data[$key] : null;
				}	
				$data['linked'] = new LinkedModel($linked_values, $this->linked_fields, $current::hasManyTableName($target));
				$models[]= new $target($data);
			}
        }
		$collection = new Collections\HasMany($models);
		Buffer::set($buffer_key, $collection);
        return $collection;
	}
}
