<?php
/**
 * Web Application Handler - Обработчик связи с моделями
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Handlers;

class Handler
{
	use \Core\Models\Wrappers\Traits\Support;
	
	protected 
		$target,
		$currmodel,
		$linked_col;
		
    /**
     * Constructor
	 * @param string $target class
	 * @param object $currmodel object
	 * @param mixed $linked_col столбец связи
     */
    public function __construct($target, $currmodel, $linked_col = null)
    {
		$this->target = $target;
		$this->currmodel = $currmodel;
		
		if (!is_string($linked_col)) {
			$cls = get_class($this->currmodel);
			$linked_col = $cls::linkedColName();
		}
		$this->linked_col = $linked_col;
	}
	
    /**
     * Get models
	 * @return Collection
     */
    public function getResource()
    {
		
	}
}
