<?php
/**
 * Web Application Wrapper - Обертка связанной младшей модели
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Wrappers;

class HasOne extends HasLinked
{
    /**
     * Get model object
	 * @return Collections\Model
     */
    public function resource()
    {
		return $this->collection->get(0);
	}
}
