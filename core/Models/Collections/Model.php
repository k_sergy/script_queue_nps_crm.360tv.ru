<?php
/**
 * Web Application Model Collection class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Collections;
use Components\Database\Connection as Db;

class Model extends \Components\Support\Collection
{
	protected $wrapper;
	protected $target;
	protected $where;
	
    /**
     * Constructor
	 * @param array $elements
	 * @param string $target class
	 * @param string $model object
     */
    public function __construct(Array $elements = [], $target = null, $model = null)
    {
        parent::__construct($elements);
		
		if (!is_null($target)) {
			
			$this->target = $target;
			
			if (!is_null($model)) {
				
				if (!($model instanceof \Core\Models\DbModel)) {
					throw new \Exception('Model must be instance of DbModel');
				}
				$wrapper = "Core\\Models\\Wrappers\\".substr(strrchr(static::class, "\\"), 1);
				if (!class_exists($wrapper)) {
					throw new \Exception('Collection wrapper not found: '.$wrapper);
				}
				$this->wrapper = new $wrapper($this, $target, $model);
			}
		}
	}
	
    /**
     * Set data where
	 * @return array
     */
    public function setWhere(array $where)
    {
		$this->where = $where;
		return $this;
	}
	
    /**
     * Get array data from models
	 * @return array
     */
    public function toArray()
    {
		$this->each(function(&$item) {
			if ($item instanceof \Core\Models\DbModel) {
				$item = $item->toArray();
			}
		});
		return $this->all();
	}
	
    /**
     * Get wrapper
	 * @return Wrapper
     */
    public function wrapper()
    {
		return $this->wrapper;
	}
	
    /**
     * Remove models
	 * @param mixed $key
	 * @return bool
     */
	public function remove($key = null)
	{
		$this->each(function($item) {
			$item->remove();
		});
	}
	
    /**
     * Remove models
	 * @param array $fields
	 * @return Collection
     */
	public function update(array $fields)
	{
		if (!is_array($this->where)) {
			throw new \Exception('Where not setted to update models');
		}
		$this->each(function($item) use($fields) {
			foreach ($fields as $field=>$val) {
				$item->{$field} = $val;
			}
		});
		$fields['modified_at'] = dateFor('Y-m-d H:i:s');
		$target_class = $this->target;
		
		foreach ($this->where as $field=>$val) {
			if (is_array($val)) {
				Db::link()->where($field, $val[0], $val[1]);
			} elseif (is_null($val)) {
					Db::link()->where($field, null, 'IS');
			} else {
				Db::link()->where($field, $val);
			}
		}
		Db::link()->update($target_class::getTableName(), $fields);
		if (Db::link()->getLastErrno() !== 0) {
			throw new \Exception('Error update models: '.Db::link()->getLastError());
		}
		return $this;
	}
}
