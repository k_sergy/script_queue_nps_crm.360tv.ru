<?php
/**
 * Web Application Trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Traits;

trait Singleton
{
	protected static
		$_instances = [];

    /**
     * Get instance
	 * @return object
     */
    public static function getInstance()
    {
		if (!isset(static::$_instances[static::class])) {
			static::setInstance(new static);
		}
		return static::$_instances[static::class];
	}
	
    /**
     * Set instance
	 * @param object $instance
	 * @return object
     */
    private static function setInstance($instance)
    {
		return static::$_instances[static::class] = $instance;
	}
}
