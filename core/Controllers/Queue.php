<?php
/**
 * Cron Jobs controller
 */
namespace Core\Controllers;
use Core\Models\QueueJob;

class Queue extends Controller
{
	const LIFE_TIME = 58;
	const MAX_COUNT = 6;
	const MAX_THREAD = 1;
	const SLEEP_THREAD = 0.25;
	const SLEEP_CYCLE = 0.75;
	
	protected $queue;
	protected $route_path = '/queue/jobs';
	protected $log_path = 'cron/queue';
	
    /**
     * Constructor
     */
    public function __construct() 
	{
		parent::__construct();
		
		$this->queue = \Components\Queue::getInstance();
	}
	
	/**
	 * Get and launch jobs
	 */
    public function process()
    {
		$l = logger($this->log_path.'/process.log');
		$start_time = time();
		$i = 0;
		$l->log(
			'Start process '.$start_time,
			'Repair jobs: '.$this->queue->checkJobs()->count()
		);
		while(true) {
			$i++;
			if ((time()-$start_time) >= static::LIFE_TIME) {
				break;
			}
			$overs = $this->queue->getOvers(static::MAX_THREAD);
			$jobs = $this->queue->getAviableJobs(array_keys($overs), static::MAX_COUNT);

			foreach($jobs->all() as $job) {
				if ((time()-$start_time) < static::LIFE_TIME) {
					$l->log(
						'Run job: '.$job->id
					); 
					$job->setState(2);
					if (!$pid = $this->background($job)) {
						$l->log('Job run error, state back'); 
						$job->setState(0);
					}
					if ((time()-$start_time) < static::LIFE_TIME) {
						msleep(
							static::SLEEP_THREAD
						);
					}
				}
			};
			if ((time()-$start_time) < static::LIFE_TIME) {
				msleep(
					static::SLEEP_CYCLE
				);
			}
 			$l->log('Iteration completed: '.$i);
		}
		$l->log('End process '.$start_time.', iterations: '.$i); 

		ajaxSuccess([
			'iterations' => $i
		]);
	}
	
    /**
     * Run background queue process
	 * @param QueueJob $qJob
	 * @return integer|bool
     */
    protected function background(QueueJob $qJob)
    {
		$date = date('Ymd');
        if (!$pid = exec(config('queue.php_command').' -d max_execution_time='.$date.' -f '.ROOT.'/index.php '.$this->route_path.'/'.$qJob->id.'/work > /dev/null 2>&1 & echo $!')) {
			return false;
		}
		exec('ps -p '.$pid.' -o command=', $output);
        if (empty($output)) {
            return false;
        }
        if (preg_match('/'.config('queue.php_command').' -d max_execution_time='.$date.'/', $output[0])) {
            return $pid;
        }
        return false;
	}
	
	/**
	 * Run job in background
	 * @param integer $job_id
	 */
    public function work($job_id)
    {
		if (!$queueJob = $this->queue->find($job_id)) {
			throw new \Exception('Queue job not found: '.$job_id);
		}
		$l = logger($this->log_path.'/'.$queueJob->name.'.log');
		try {
			$this->queue->process($queueJob);
			if ($queueJob->hasFailed()) {
				$e = $queueJob->getException();
				$l->log(
					'Job: '.$queueJob->id.' attempt: '.$queueJob->attempt,
					'State: '.$queueJob->state,
					'Exception: '.$e->getMessage(),
					$e->getFile().'('.$e->getLine().')'
				);
				error_log('Queue Job '.$queueJob->id." exception: ".$e->getMessage().", in: ".$e->getFile().'('.$e->getLine().')');
			} else {
				$l->log(
					'Job: '.$queueJob->id.' success', 
					'Attempt: '.$queueJob->attempt,
					'State: '.$queueJob->state
				);
			}
		} catch(\Exception $e) {
			$l->log(
				'Job: '.$queueJob->id.' exception', 
				$e->getMessage(),
				$e->getFile().'('.$e->getLine().')'
			);
			$queueJob->setState(0);
			error_log('Queue Job '.$queueJob->id." exception: ".$e->getMessage().", in: ".$e->getFile().'('.$e->getLine().')');
		}
		if ($queueJob->hasCompleted()) {
			$l->log('Job: '.$queueJob->id.' completed');
		}
		ajaxSuccess([
			'job' => $queueJob->toArray()
		]);
	}
}
