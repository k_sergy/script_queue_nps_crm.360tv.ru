<?php
/**
 * Web Application User authenticator
 */
namespace Core\Controllers;
use Components\Auth;

class Authenticate extends Controller
{
	/**
	 * Login page
	 * @param array $errors
	 */
    public function loginPage($errors = [])
    {
		view('main.login', [
			'title' => 'Вход',
			'auth' => app('auth'),
			'errors' => (object)$errors
		]);
	}
	
	/**
	 * Login user
	 * @param Auth $auth
	 */
    public function attempt(Auth $auth)
    {
		$validator = validator($this->request->post(), [
			'login' => 'required|string|minl:1|maxl:100',
			'password' => 'required|string|minl:1|maxl:64',
			'remember' => 'optional',
		]);
		if (!$validator->isValid()) {
			return $this->loginPage(
				$validator->errors()
			);
		}
		$validated = $validator->data();
		$user_data = ['login' => $validated->login, 'password' => $validated->password, 'active' => 1];
		
		if (!$auth->attempt($user_data, (bool)$validated->remember)) {
			$validator->error('login', '');
			$validator->error('password', 'Неудачная попытка входа');
			return $this->loginPage(
				$validator->errors()
			);
		}
		to('Main');
	}
	
	/**
	 * Logout user
	 * @param array $errors
	 */
    public function logout()
    {
		$auth->logout();
		to('LoginPage');
	}
}
