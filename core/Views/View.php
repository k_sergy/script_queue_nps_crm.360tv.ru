<?php
/**
 * Web Application View class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Views;

class View
{
    private $tpl = 'main',
			$content = 'index';
    /**
     * Set template
	 * @param string $name tpl filename
	 * @return void
     */
    public function tpl($name)
    {
        $this->tpl = $name;
    }

    /**
     * Content file
	 * @param string $name tpl content filename
	 * @return void
     */
    public function contentFile($name)
    {
        $this->content = $name;
    }

    /**
     * Template generator
	 * @param mixed $data tpl data
	 * @return void
     */
    public function generate($data = ['title' => ''])
    {
		if (!file_exists(RESOURCES.'/views/'.$this->tpl.'.php')) {
			throw new \Exception('Error load view template: '.$this->tpl);
		}
		if (is_array($data)) {
			extract($data);
		}
        include RESOURCES.'/views/'.$this->tpl.'.php';
    }
	
    /**
     * Include Content file
	 * @param string $filename tpl content filename
	 * @return void
     */
    public function content($filename = null, $data = [])
    {
        if (is_null($filename)) {
			$filename = $this->content;
		}
		if (!file_exists(RESOURCES.'/views/'.$filename.'.php')) {
			throw new \Exception('Error load view content file: '.$filename);
		}
		if (is_array($data)) {
			extract($data);
		}
		include RESOURCES.'/views/'.$filename.'.php';
    }
}
