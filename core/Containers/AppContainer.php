<?php
/**
 * Web Application specific container  
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Containers;
use Core\Traits;

class AppContainer
{
	use Traits\Singleton;
	
    /**
     * Constructor
     */
    protected function __construct()
    {
		call_user_func_array(
			[$this, '_boot'], getMethodRegisteredArgs($this, '_boot')
		);
	}
	
    /**
     * Application shutdown
     */
	public function __shutdown()
	{
		call_user_func_array(
			[$this, '_shutdown'], getMethodRegisteredArgs($this, '_shutdown')
		);
	}
}
