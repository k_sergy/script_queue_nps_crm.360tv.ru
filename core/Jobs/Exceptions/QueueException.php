<?php
/**
 * Web Application Job Exception
 */
namespace Core\Jobs\Exceptions;

class QueueException
{
	protected $code;
	protected $message;
	protected $file;
	protected $line;
	protected $trace;
	
    /**
     * Constructor
	 * @param Exception $e
     */
	public function __construct(\Exception $e)
	{
		$this->code = $e->getCode();
		$this->message = $e->getMessage();
		$this->file = $e->getFile();
		$this->line = $e->getLine();
		$this->trace = [];
	}
	
	/**
	 * Get exception code
	 * @return integer
	 */
    public function getCode()
    {
		return $this->code;
    }
	
	/**
	 * Get exception message
	 * @return string
	 */
    public function getMessage()
    {
		return $this->message;
    }
	
	/**
	 * Get exception file
	 * @return string
	 */
    public function getFile()
    {
		return $this->file;
    }
	
	/**
	 * Get exception line
	 * @return string
	 */
    public function getLine()
    {
		return $this->line;
    }
	
	/**
	 * Get exception trace
	 * @return array
	 */
    public function getTrace()
    {
		return $this->trace;
    }
}
