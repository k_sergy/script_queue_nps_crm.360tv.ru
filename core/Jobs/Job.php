<?php
/**
 * Web Application Job
 */
namespace Core\Jobs;

class Job 
{
	protected $qjob_id;
	protected $exception;
	
    /**
     * Has job failed
	 * @return bool
     */
	public function hasFailed()
	{
		return !is_null($this->exception);
	}
	
	/**
	 * Set qjob id
	 * @param QueueJob $qjob
	 */
    public function setQueue(\Core\Models\QueueJob $qjob)
    {
		$this->qjob_id = $qjob->id;
    }
	
	/**
	 * Handle job exception
	 * @param Exception $e
	 */
    public function setException(\Exception $e)
    {
		$this->exception = new Exceptions\QueueException($e);
		if (method_exists($this, 'failed')) {
			$this->failed($this->exception);
		}
    }
	
	/**
	 * Get job exception
	 * @return Exception
	 */
    public function getException()
    {
		return $this->exception;
    }
	
	/**
	 * Remove job exception
	 * @return Job
	 */
    public function removeException()
    {
		$this->exception = null;
		return $this;
    }
}
