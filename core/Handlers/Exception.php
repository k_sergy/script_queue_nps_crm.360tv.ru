<?php
/**
 * Web Application Exception handler
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Handlers;
use Components\Response;

class Exception extends \Exception
{
    public function __construct($message, $code = 0, $previous = null)
    {
		parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
		$trace = $this->stringTrace();
		error_log(
			'Exception: '.strip_tags($trace)
		);
		http_response_code(500);
		Response::html(
			strtr(file_get_contents(RESOURCES.'/templates/error/1.html'), [
					'{trace}' => $trace
				]	
			)
		);
    }
	
    protected function stringTrace()
    {
		$trace = [];
		$stack = array_reverse($this->getTrace());

		if (count($stack) == 1 && isset($stack[0]['function']) && $stack[0]['function'] == 'exception_handler') {
			$exception = $stack[0]['args'][0];
			$stack = array_reverse($exception->getTrace());
			$stack[]= array(
				'file' => $exception->getFile(),
				'line' => $exception->getLine(),
			);
		}
		foreach ($stack as $k=>$itm) {

			if(!isset($itm['args'])) {
				$itm['args'] = [];
			}
			if (isset($itm['file'], $itm['line'], $itm['function'], $itm['class'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['class'].$itm['type'].$itm['function'].'(<span class="args">'.static::argsToString($itm['args']).'</span>)</b>';
			
			} else if (isset($itm['file'], $itm['line'], $itm['class'], $itm['type'])) {
			
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';

			} else if (isset($itm['file'], $itm['line'], $itm['function'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['function'].'(<span class="args">'.static::argsToString($itm['args']).'</span>)</b>';
				
			} else if (isset($itm['class'], $itm['function'], $itm['type'])) {
				
				$str = 'called: <b>'.$itm['class'].$itm['type'].$itm['function'].'(<span class="args">'.static::argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['file'], $itm['line'], $itm['function'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['function'].'(<span class="args">'.static::argsToString($itm['args']).'</span>)</b>';
				
			} else if (isset($itm['file'], $itm['line'])) {
				
				$str = 'in '.str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';
				
			} else if (isset($itm['function'])) {
				
				$str = 'called: <b>'.$itm['function'].'(<span class="args">'.static::argsToString($itm['args']).'</span>)</b>';
				
			} else {
				$str = $str = '<b>-</b>';
			}
			$trace[]= '<div class="line">['.$k.'] '.$str."</div>";
		}
		$trace[]= '<div class="excMsg">'.$this->getMessage().'</div>';

		return "\n".implode("\n", $trace);
	}
	
    public static function argsToString($args = [])
    {
		$arg_list = [];
		foreach ($args as $arg) {
			$type = gettype($arg);
			if (!is_string($arg) && !is_numeric($arg) && !is_double($arg) && !is_bool($arg) && !is_int($arg) && !is_null($arg)) {
				$argument = ucfirst($type);
				if (is_array($arg)) {
					$argument .= '['.count($arg).']';
				}
			} else {
				if (is_string($arg)) {
					$argument = '<i>' . $type . "</i> '" . str_replace(ROOT, '...', mb_substr($arg, 0, 1500))."'";
				} else {
					$argument = '<i>' . $type . "</i> '" . str_replace(ROOT, '...', (string)$arg)."'";
				}
			}
			$arg_list[]= $argument;
		}
		return implode(', ', $arg_list);
	}
	
    public static function handleTrace(array $trace)
    {
		$trace = array_reverse($trace);
		$handled = [];
		foreach ($trace as $k=>$itm) {
			if(!isset($itm['args'])) {
				$itm['args'] = [];
			}
			if (isset($itm['file'], $itm['line'], $itm['function'], $itm['class'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): '.$itm['class'].$itm['type'].$itm['function'].'('.static::argsToString($itm['args']).')';
			
			} else if (isset($itm['file'], $itm['line'], $itm['class'], $itm['type'])) {
			
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';

			} else if (isset($itm['file'], $itm['line'], $itm['function'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): '.$itm['function'].'('.static::argsToString($itm['args']).')';
				
			} else if (isset($itm['class'], $itm['function'], $itm['type'])) {
				
				$str = 'called: '.$itm['class'].$itm['type'].$itm['function'].'('.static::argsToString($itm['args']).')';
				
			} else if (isset($itm['file'], $itm['line'], $itm['function'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): '.$itm['function'].'('.static::argsToString($itm['args']).')';
				
			} else if (isset($itm['file'], $itm['line'])) {
				
				$str = 'in '.str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';
				
			} else if (isset($itm['function'])) {
				
				$str = 'called: '.$itm['function'].'('.static::argsToString($itm['args']).')';
				
			} else {
				$str = $str = '-';
			}
			$handled[]= $str;
		}
		return $handled;
	}
}
