<?php
/**
 * Web Application Error handler
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Handlers;
use Components\Support\Str;
use Components\Response;

class Error
{
	protected $msg;
	protected $title;
	protected $trace;
	protected $error;
	protected $code = 0;
	protected $titles = [
				  0 => 'App Error',
				  1 => 'App Exception',
				  2 => 'Error',
				400 => 'Bad Request',
				401 => 'Unauthorized',
				403 => 'Forbidden',
				404 => 'Not Found',
				500 => 'Internal Server Error',
				503 => 'Service Unavailable'
			];
	
    /**
     * Constructor
     */
    public function __construct($code = null, $msg = null, $error = null)
    {
		if (is_string($code)) {
			$msg = $code;
			$code = 3;
		}
		$this->code = (int)$code;
		$this->title = $this->getTitle($this->code);
		$this->msg = Str::toString($msg);
		$this->error = $this->getTitle($this->code);
		if (!is_null($error)) {
			$this->error = Str::toString($error);
		}
		$this->trace = $this->stringTrace();
		error_log(
			$this->error.' '.strip_tags($this->trace)."\n".$this->msg
		);
		$this->render();
	}
	
    /**
     * Get string backtrace
	 * @return string
     */
    protected function stringTrace()
    {
		$trace = [];
		$stack = array_reverse(array_slice(app()->getTrace(), 2));
		$end = array_pop($stack);

		foreach ($stack as $k=>$itm) {

			if(!isset($itm['args'])) {
				$itm['args'] = [];
			}
			if (isset($itm['file'], $itm['line'], $itm['function'], $itm['class'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['class'].$itm['type'].$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
			
			} else if (isset($itm['file'], $itm['line'], $itm['class'], $itm['type'])) {
			
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';

			} else if (isset($itm['file'], $itm['line'], $itm['function'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['class'], $itm['function'], $itm['type'])) {
				
				$str = 'called: <b>'.$itm['class'].$itm['type'].$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['file'], $itm['line'], $itm['function'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['file'], $itm['line'])) {
				
				$str = 'in '.str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';
				
			} else if (isset($itm['function'])) {
				
				$str = 'called: <b>'.$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else {
				$str = $str = '<b>-</b>';
			}
			$trace[]= '<div class="line">['.$k.'] '.$str."</div>";
		}
		if (isset($end['file']) && isset($end['line'])) {
			$trace[]= '<div class="line">['.++$k.'] '.'in '.str_replace(ROOT, '...', $end['file']).'('.$end['line'].'):'."</div>";
		}
		return "\n".implode("\n", $trace);
	}
	
    /**
     * Convert args to string
	 * @param array $args
	 * @return string
     */
    protected function argsToString($args = [])
    {
		$arg_list = [];
		foreach ($args as $arg) {
			$type = gettype($arg);
			if (!is_string($arg) && !is_numeric($arg) && !is_double($arg) && !is_bool($arg) && !is_int($arg) && !is_null($arg)) {
				$argument = ucfirst($type);
				if (is_array($arg)) {
					$argument .= '['.count($arg).']';
				}
			} else {
				if (is_string($arg)) {
					$argument = '<i>' . $type . "</i> '" . str_replace(ROOT, '...', mb_substr($arg, 0, 1500))."'";
				} else {
					$argument = '<i>' . $type . "</i> '" . str_replace(ROOT, '...', (string)$arg)."'";
				}
			}
			$arg_list[]= $argument;
		}
		return '<span class="args">'.implode(', ', $arg_list).'</span>';
	}
	
    /**
     * Error title from
	 * @param integer $code Error code
	 * @return string title
     */
    protected function getTitle($code)
    {
		if (!isset($this->titles[$code])) {
			return 'Untitled error';
		}
		return $this->titles[$code];
	}

    /**
     * Render error page
     */
    protected function render()
    {
		$error_tpl = 'templates/error/'.$this->code.'.html';
		if (!file_exists(RESOURCES.'/'.$error_tpl)) {
			$error_tpl = 'templates/error/page.html';
		}
		$replace = array(
			'{trace}' => $this->trace,
			'{title}' => $this->title,
			'{code}' => $this->code,
			'{msg}' => $this->msg,
			'{error}' => $this->error
		);
		http_response_code($this->getResponseCode());
		Response::html(
			strtr(file_get_contents(RESOURCES.'/'.$error_tpl), [
						'{trace}' => $this->trace,
						'{title}' => $this->title,
						'{code}' => $this->code,
						'{error}' => $this->error,
						'{msg}' => $this->msg
					]	
				)
		);
	}
	
    /**
     * Get http response code
	 * @return integer
     */
    protected function getResponseCode()
    {
		if (in_array($this->code, [0, 1])) {
			return 500;
		}
		if (strlen($this->code) == 3) {
			return $this->code;
		}
		return 200;
	}
}
